﻿namespace Oblaka.PeerReview.Web {
  using System.Web.Mvc;

  using Autofac;
  using Autofac.Integration.Mvc;

  using Microsoft.WindowsAzure;

  using Oblaka.PeerReview.Services;
  using Oblaka.PeerReview.Services.Azure;
  using Oblaka.PeerReview.Services.Excel;
  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// Class for resolving dependencies on services and factories.
  /// </summary>
  internal static class DependenciesConfig {
    #region Public Properties

    /// <summary>
    /// Gets the container.
    /// </summary>
    public static IContainer Container { get; private set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Sets the dependency resolver.
    /// </summary>
    public static void SetAsDependencyResolver() {
      var builder = new ContainerBuilder();
      builder.RegisterControllers(typeof(DependenciesConfig).Assembly);

      builder.RegisterType<ApplicantFactory>().As<IApplicantFactory>().SingleInstance();
      builder.RegisterType<ArrayService>().As<IArrayService>();
      builder.RegisterType<GraphService>().As<IGraphService>().SingleInstance();
      builder.RegisterType<ExcelFactory>().As<IExcelFactory>().SingleInstance();
      builder.RegisterType<AssignmentService>().As<IAssignmentService>().SingleInstance();
      builder.RegisterType<ScoreSheetService>().As<IScoreSheetService>().SingleInstance();
      var geoConnectionString = CloudConfigurationManager.GetSetting("GeoStorageConnectionString");
      var geoFilePath = CloudConfigurationManager.GetSetting("GeoStorageFilePath");
      var geoStorage = !string.IsNullOrEmpty(geoConnectionString)
                         ? new AzureGeoStorage(geoConnectionString)
                         : !string.IsNullOrEmpty(geoFilePath)
                             ? new FileGeoStorage(geoFilePath)
                             : (IGeoStorage)new NonPersistantGeoStorage();
      builder.RegisterInstance(geoStorage);
      builder.RegisterInstance(new CodeOnceGeoService(new PdokService(), geoStorage)).As<IGeoService>();

      Container = builder.Build();
      DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));
    }

    #endregion
  }
}