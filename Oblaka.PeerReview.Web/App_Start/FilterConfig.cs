﻿namespace Oblaka.PeerReview.Web {
  using System.Diagnostics.CodeAnalysis;
  using System.Web.Mvc;

  [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", 
    Justification = "Reviewed. Suppression is OK here.")]
  public class FilterConfig {
    #region Public Methods and Operators

    public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
      filters.Add(new HandleErrorAttribute());
    }

    #endregion
  }
}