﻿namespace Oblaka.PeerReview.Web {
  using System.Diagnostics.CodeAnalysis;
  using System.Web.Http;

  [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", 
    Justification = "Reviewed. Suppression is OK here.")]
  public static class WebApiConfig {
    #region Constants

    public const string UrlPrefixAbsolute = "/api/";

    #endregion

    #region Public Methods and Operators

    public static void Register(HttpConfiguration config) {
      // Web API configuration and services

      // Web API routes
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
    }

    #endregion
  }
}