﻿namespace Oblaka.PeerReview.Web.Models {
  /// <summary>
  /// The status model.
  /// </summary>
  public class StatusModel {
    #region Public Properties

    /// <summary>
    /// Gets or sets the error.
    /// </summary>
    public string Error { get; set; }

    /// <summary>
    /// Gets or sets the message.
    /// </summary>
    public string Message { get; set; }

    #endregion
  }
}