﻿namespace Oblaka.PeerReview.Web.Controllers {
  using System;
  using System.Diagnostics.CodeAnalysis;
  using System.IO;
  using System.Web;
  using System.Web.Mvc;

  using Oblaka.PeerReview.Services;

  [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", 
    Justification = "Reviewed. Suppression is OK here.")]
  public class HomeController : Controller {
    #region Fields

    private readonly IAssignmentService assignmentService;

    private readonly IExcelFactory excelFactory;

    private readonly IScoreSheetService scoreSheetService;

    #endregion

    #region Constructors and Destructors

    public HomeController(
      IExcelFactory excelFactory, 
      IAssignmentService assignmentService, 
      IScoreSheetService scoreSheetService) {
      this.excelFactory = excelFactory;
      this.assignmentService = assignmentService;
      this.scoreSheetService = scoreSheetService;
    }

    #endregion

    #region Delegates

    private delegate bool TryWorkbookAction(IExcelWorkbook excel, out string message);

    #endregion

    #region Properties

    private object RequestProperties {
      get {
        return
          new {
                this.HttpContext.Request.RawUrl, 
                RemoteAddr = this.HttpContext.Request.ServerVariables["REMOTE_ADDR"], 
                SessionID = this.HttpContext.Session == null ? null : this.HttpContext.Session.SessionID
              };
      }
    }

    #endregion

    #region Public Methods and Operators

    public ActionResult Hulp() {
      this.ViewBag.Title = "Over peer review";

      return this.View("Text", model: "Hulp");
    }

    public ActionResult Index() {
      this.ViewBag.Title = "Peer review";

      return this.View();
    }

    [HttpPost]
    public ActionResult Toewijzen(HttpPostedFileBase excel) {
      return this.Execute(this.assignmentService.TryAddAssignment, "Toewijzen", "toegewezen", excel);
    }

    [HttpPost]
    public ActionResult Verwerken(HttpPostedFileBase excel) {
      return this.Execute(this.scoreSheetService.TryAddScoreSheet, "Verwerken", "verwerkt", excel);
    }

    #endregion

    #region Methods

    [HttpPost]
    private ActionResult Execute(TryWorkbookAction action, string activity, string filename, HttpPostedFileBase excel) {
      try {
        Func<FileStreamResult> func = () => {
          this.Session["error"] = null;
          this.Session["message"] = null;
          if (excel == null || excel.ContentLength == 0 || string.IsNullOrEmpty(excel.FileName)
              || !excel.FileName.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase)
              || excel.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            throw new InvalidDataException("Geen Excel document (.xlsx) ingevoerd.");
          }

          var workbook = this.excelFactory.Read(excel.InputStream);
          string message;
          if (!action(workbook, out message)) {
            throw new InvalidDataException(message);
          }

          this.Session["message"] = message;
          this.Response.AddHeader(
            "Content-Disposition", 
            string.Format("attachment; filename=\"peer-review-{0}.xlsx\"", filename));
          return new FileStreamResult(
            workbook.Data, 
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        };
        return this.ExecuteWithLog(func, activity, this.RequestProperties);
      }
      catch (Exception e) {
        this.Session["error"] = e.Message;
        this.ViewBag.Title = "Fout: " + e.Message;
        return this.View("Text", model: "Leeg");
      }
    }

    #endregion
  }
}