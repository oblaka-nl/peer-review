﻿namespace Oblaka.PeerReview.Web.Controllers {
  using System.Diagnostics.CodeAnalysis;
  using System.Web;
  using System.Web.Http;

  using Oblaka.PeerReview.Web.Models;

  [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", 
    Justification = "Reviewed. Suppression is OK here.")]
  public class StatusController : ApiController {
    public StatusModel Get() {
      var result = new StatusModel();
      var context = this.Request.Properties["MS_HttpContext"] as HttpContextBase;
      var session = context == null ? null : context.Session;
      if (session == null) {
        return result;
      }

      result.Error = session["error"] as string;
      result.Message = session["message"] as string;
      return result;
    }

    [HttpPost]
    [Route("api/Status/Reset")]
    public IHttpActionResult Reset() {
      var context = this.Request.Properties["MS_HttpContext"] as HttpContextBase;
      var session = context == null ? null : context.Session;
      if (session != null) {
        session.Clear();
      }

      return this.Ok();
    }
  }
}