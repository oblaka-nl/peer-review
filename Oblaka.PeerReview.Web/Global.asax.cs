﻿namespace Oblaka.PeerReview.Web {
  using System.Diagnostics.CodeAnalysis;
  using System.Web;
  using System.Web.Http;
  using System.Web.Mvc;
  using System.Web.Optimization;
  using System.Web.Routing;
  using System.Web.SessionState;

  [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", 
    Justification = "Reviewed. Suppression is OK here.")]
  public class WebApiApplication : HttpApplication {
    #region Methods

    protected void Application_Start() {
      DependenciesConfig.SetAsDependencyResolver();
      GlobalConfiguration.Configure(WebApiConfig.Register);
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }

    protected void Application_PostAuthorizeRequest() {
      if (this.IsWebApiRequest()) {
        HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.ReadOnly);
      }
    }

    private bool IsWebApiRequest() {
      return HttpContext.Current.Request.Url.AbsolutePath.StartsWith(WebApiConfig.UrlPrefixAbsolute);
    }

    #endregion
  }
}