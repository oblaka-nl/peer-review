﻿jQuery(function ($) {
    function submitExcel() {
        var div = $(this).addClass("hidden").nextAll("div").removeClass("hidden");

        function showMessage(status) {
            if (!status.Error && !status.Message) {
                window.setTimeout(findStatus, 200);
                return;
            }

            div.first()
                .data("orig", div.first().text())
                .removeClass("alert-danger alert-warning alert-success")
                .addClass(status.Error ? "alert-danger" : "alert-success")
                .text(status.Error || status.Message);
            $.post("/api/Status/Reset");
        }

        function findStatus() {
            $.getJSON("/api/Status", { date: new Date().getTime() }, showMessage);
        }

        findStatus();
    }

    function changeExcel() {
        $(this).closest("form").submit();
    }

    function resetForm() {
        var ancestor = $(this).parent().parent().children("form, div").toggleClass("hidden");
        var firstDiv = ancestor.filter("div:first").removeClass("alert-danger alert-success").addClass("alert-warning");
        firstDiv.text(firstDiv.data("orig"));
        ancestor.find(":file").val(null);
    }

    $(document).on('change', '.btn-file :file', changeExcel);
    $(document).on('submit', 'form:has(:file)', submitExcel);
    $(document).on('click', 'button.btn-info', resetForm);
});