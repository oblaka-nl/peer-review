﻿namespace Oblaka.PeerReview.Services.Tests {
  using System;
  using System.Collections.Generic;
  using System.Diagnostics;
  using System.Linq;

  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The algorithm tests.
  /// </summary>
  [TestFixture]
  [Explicit]
  public class AlgorithmTests {
    #region Properties

    /// <summary>
    /// Gets the applicant factory.
    /// </summary>
    private IApplicantFactory ApplicantFactory {
      get {
        return
          new ApplicantFactory(
            new CodeOnceGeoService(new PdokService(), new FileGeoStorage(@"..\..\..\GeoStorage.txt")));
      }
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Generates demo data.
    /// </summary>
    [Test]
    [Explicit]
    public void GenerateDemo() {
      var arrayService = new ArrayService(33);
      var names = arrayService.Sample(Lists.GivenNames, 30);
      arrayService.Shuffle(names);
      var localities = arrayService.Sample(Lists.LocalitiesBrabant, 30);
      arrayService.Shuffle(localities);
      this.AlgorithmTest(names, localities, arrayService, TimeSpan.FromSeconds(1));
    }

    /// <summary>
    /// The large test.
    /// </summary>
    [Test]
    public void LargeTest() {
      var arrayService = new ArrayService(34);
      var names = arrayService.Sample(Lists.GivenNames, 400);
      this.AlgorithmTest(names, Lists.LocalitiesBrabant, arrayService, TimeSpan.FromSeconds(5));
    }

    /// <summary>
    /// The small test.
    /// </summary>
    [Test]
    public void SmallTest() {
      var arrayService = new ArrayService(33);
      var names = arrayService.Sample(Lists.GivenNames, 20);
      var localities = arrayService.Sample(Lists.LocalitiesBrabant, 8);
      this.AlgorithmTest(names, localities, arrayService, TimeSpan.FromSeconds(1));
    }

    /// <summary>
    /// The uneven test.
    /// </summary>
    [Test]
    public void UnevenTest() {
      var arrayService = new ArrayService(33);
      var names = arrayService.Sample(Lists.GivenNames, 23);
      var localities = arrayService.Sample(Lists.LocalitiesBrabant, names.Length);
      this.AlgorithmTest(names, localities, arrayService, TimeSpan.FromSeconds(1));
    }

    #endregion

    #region Methods

    /// <summary>
    /// Tests the algorithm
    /// </summary>
    /// <param name="names">
    /// The given names.
    /// </param>
    /// <param name="localities">
    /// The localities.
    /// </param>
    /// <param name="arrayService">
    /// The array service
    /// </param>
    /// <param name="timeout">
    /// The timeout.
    /// </param>
    private void AlgorithmTest(string[] names, string[] localities, ArrayService arrayService, TimeSpan timeout) {
      var applicantFactory = this.ApplicantFactory;
      var applicants = (from name in names
                        let locality = arrayService.Sample(localities, 1)[0]
                        select
                          applicantFactory.CreateApplicant(string.Format("{0} van {1}", name, locality), null, locality))
        .ToArray();
      var edges = (from i in Enumerable.Range(0, applicants.Length)
                   select
                     (from j in Enumerable.Range(0, applicants.Length)
                      where applicants[i].CanReview(applicants[j])
                      select j).ToArray()).ToArray();
      var sw = Stopwatch.StartNew();
      var matching = new GraphService(arrayService).GetNonReciprocicalMatchings(edges, 5);
      sw.Stop();
      Assert.IsNotNull(matching);
      Assert.Less(sw.Elapsed, timeout);
      var seen = new HashSet<Tuple<string, string>>();
      foreach (var i in Enumerable.Range(0, applicants.Length)) {
        var others = (from j in Enumerable.Range(0, 5) select applicants[matching[i, j]].Id).ToList();
        Assert.IsFalse(others.Contains(applicants[i].Id));
        Assert.AreEqual(5, others.Distinct().Count());
        foreach (var other in others) {
          seen.Add(new Tuple<string, string>(applicants[i].Id, other));
          Assert.IsFalse(
            seen.Contains(new Tuple<string, string>(other, applicants[i].Id)), 
            "The matching should be non-reciprocical");
        }
      }
    }

    #endregion
  }
}