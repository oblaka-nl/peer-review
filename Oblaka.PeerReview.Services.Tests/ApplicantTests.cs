﻿namespace Oblaka.PeerReview.Services.Tests {
  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The Applicant tests.
  /// </summary>
  [TestFixture]
  [Explicit]
  public class ApplicantTests {
    #region Properties

    /// <summary>
    /// Gets the applicant factory.
    /// </summary>
    private IApplicantFactory ApplicantFactory {
      get {
        return new ApplicantFactory(new CodeOnceGeoService(new PdokService(), new NonPersistantGeoStorage()));
      }
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tests that applicant can review work of distant other.
    /// </summary>
    [Test]
    public void CanReviewDistantTest() {
      var applicant = this.ApplicantFactory.CreateApplicant("me", "5616HZ", "Eindhoven");
      var other = this.ApplicantFactory.CreateApplicant("you", null, "Breda");
      Assert.IsTrue(applicant.CanReview(other));
    }

    /// <summary>
    /// Tests that applicant can review work of nearby locality if postal codes are distant.
    /// </summary>
    [Test]
    public void CanReviewPostalCodesTest() {
      var applicant = this.ApplicantFactory.CreateApplicant("me", "5629NG", "Eindhoven");
      var other = this.ApplicantFactory.CreateApplicant("you", "5521NP", "Steensel");
      Assert.IsTrue(applicant.CanReview(other));
    }

    /// <summary>
    /// Tests that applicant cannot review work of nearby locality.
    /// </summary>
    [Test]
    public void CannotReviewLocalitiesTest() {
      var applicant = this.ApplicantFactory.CreateApplicant("me", null, "Eindhoven");
      var other = this.ApplicantFactory.CreateApplicant("you", null, "Steensel");
      Assert.IsFalse(applicant.CanReview(other));
    }

    /// <summary>
    /// Tests that applicant cannot review his own work.
    /// </summary>
    [Test]
    public void CannotReviewSelfTest() {
      var applicant = this.ApplicantFactory.CreateApplicant("me", "5616HZ", "Eindhoven");
      Assert.IsFalse(applicant.CanReview(applicant));
    }

    #endregion
  }
}