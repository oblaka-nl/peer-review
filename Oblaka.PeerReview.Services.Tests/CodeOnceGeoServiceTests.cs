﻿namespace Oblaka.PeerReview.Services.Tests {
  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The code once geo service tests.
  /// </summary>
  [TestFixture]
  [Explicit]
  public class CodeOnceGeoServiceTests : GeoServiceTests {
    #region Properties

    /// <summary>
    /// Gets the geo service.
    /// </summary>
    protected override IGeoService GeoService {
      get {
        return new CodeOnceGeoService(new PdokService(), new NonPersistantGeoStorage());
      }
    }

    #endregion
  }
}