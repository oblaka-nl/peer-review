﻿namespace Oblaka.PeerReview.Services.Tests {
  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The PointExtensions tests.
  /// </summary>
  [TestFixture]
  public class PointExtensionsTests {
    #region Public Methods and Operators

    /// <summary>
    /// Tests whether hypotenusa of triangle with sides 3 and 4 equals 5.
    /// </summary>
    [Test]
    public void Test345Triangle() {
      var one = new Point { Position = "3 4" };
      var other = new Point { Position = "0 0" };
      Assert.AreEqual(5M, one.DistanceTo(other));
    }

    #endregion
  }
}