﻿namespace Oblaka.PeerReview.Services.Tests.Pdok {
  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The PdokService tests.
  /// </summary>
  [TestFixture]
  [Explicit]
  public class PdokServiceTests : GeoServiceTests {
    #region Properties

    /// <summary>
    /// Gets the geo service.
    /// </summary>
    protected override IGeoService GeoService {
      get {
        return new PdokService();
      }
    }

    #endregion
  }
}