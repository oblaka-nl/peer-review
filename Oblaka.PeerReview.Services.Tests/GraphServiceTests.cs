﻿namespace Oblaka.PeerReview.Services.Tests {
  using System.Linq;

  using NUnit.Framework;

  /// <summary>
  /// The GraphService tests.
  /// </summary>
  [TestFixture]
  public class GraphServiceTests {
    #region Public Methods and Operators

    /// <summary>
    /// The harder asymmetric matching test.
    /// </summary>
    [Test]
    public void HarderAsymmetricMatchingTest() {
      var edges = new[] { new[] { 1, 2 }, new[] { 0, 2 }, new[] { 0, 1 } };
      this.AsymmetricMatchingTest(edges, 2);
    }

    /// <summary>
    /// The harder symmetric matching test.
    /// </summary>
    [Test]
    public void HarderSymmetricMatchingTest() {
      var edges = new[] { new[] { 1, 2, 3 }, new[] { 0, 2, 3 }, new[] { 0, 1, 3 }, new[] { 0, 1, 2 } };
      this.SymmetricMatchingTest(edges, 3);
    }

    /// <summary>
    /// The simple asymmetric matching test.
    /// </summary>
    [Test]
    public void SimpleAsymmetricMatchingTest() {
      var edges = new[] { new[] { 1 }, new[] { 0 } };
      this.AsymmetricMatchingTest(edges, 1);
    }

    /// <summary>
    /// The simple symmetric matching test.
    /// </summary>
    [Test]
    public void SimpleSymmetricMatchingTest() {
      var edges = new[] { new[] { 1 }, new[] { 0 } };
      this.SymmetricMatchingTest(edges, 1);
    }

    #endregion

    #region Methods

    /// <summary>
    /// Performs asymmetric matching test.
    /// </summary>
    /// <param name="edges">
    /// The edges.
    /// </param>
    /// <param name="count">
    /// The count.
    /// </param>
    private void AsymmetricMatchingTest(int[][] edges, int count) {
      var result = new GraphService(new ArrayService(33)).GetAsymmetricMatchings(edges, count);
      Assert.AreEqual(edges.Length, result.GetLength(0));
      Assert.AreEqual(count, result.GetLength(1));
      var counters = new int[edges.Length];
      foreach (var i in Enumerable.Range(0, edges.Length)) {
        // All matches are distinct
        Assert.AreEqual(count, Enumerable.Range(0, count).Select(j => result[i, j]).Distinct().Count());
        foreach (var j in Enumerable.Range(0, count)) {
          // Does not match with itself
          Assert.AreNotEqual(i, result[i, j]);
          counters[result[i, j]]++;
        }
      }

      // All nodes are matched equally
      foreach (var i in Enumerable.Range(0, edges.Length)) {
        Assert.AreEqual(count, counters[i]);
      }
    }

    /// <summary>
    /// Performs symmetric matching test.
    /// </summary>
    /// <param name="edges">
    /// The edges.
    /// </param>
    /// <param name="count">
    /// The count.
    /// </param>
    private void SymmetricMatchingTest(int[][] edges, int count) {
      var result = new GraphService(new ArrayService(33)).GetSymmetricMatchings(edges, count);
      Assert.AreEqual(edges.Length, result.GetLength(0));
      Assert.AreEqual(count, result.GetLength(1));
      foreach (var i in Enumerable.Range(0, edges.Length)) {
        // All matches are distinct
        Assert.AreEqual(count, Enumerable.Range(0, count).Select(j => result[i, j]).Distinct().Count());
        foreach (var j in Enumerable.Range(0, count)) {
          // Does not match with itself
          Assert.AreNotEqual(i, result[i, j]);

          // Matching is symmetric
          Assert.AreEqual(i, result[result[i, j], j]);
        }
      }
    }

    #endregion
  }
}