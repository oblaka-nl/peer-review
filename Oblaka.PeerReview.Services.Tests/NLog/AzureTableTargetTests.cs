﻿namespace Oblaka.PeerReview.Services.Tests.NLog {
  using global::NLog;

  using NUnit.Framework;

  /// <summary>
  /// The azure table target tests.
  /// </summary>
  [TestFixture]
  [Explicit]
  public class AzureTableTargetTests {
    #region Public Methods and Operators

    /// <summary>
    /// Creates a dummy log entry.
    /// </summary>
    [Test]
    public void LogTest() {
      var logEvent = new LogEventInfo(LogLevel.Info, this.GetType().FullName, "some event");
      logEvent.Properties["aap"] = "gorilla";
      LogManager.GetLogger(logEvent.LoggerName).Log(logEvent);
    }

    #endregion
  }
}