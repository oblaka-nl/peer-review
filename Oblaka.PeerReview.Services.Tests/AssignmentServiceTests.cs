﻿namespace Oblaka.PeerReview.Services.Tests {
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;

  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Excel;
  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The assignment service tests.
  /// </summary>
  [TestFixture]
  public class AssignmentServiceTests {
    #region Properties

    /// <summary>
    /// Gets the excel factory.
    /// </summary>
    private IExcelFactory ExcelFactory {
      get {
        return new ExcelFactory();
      }
    }

    /// <summary>
    /// Gets the assignment service.
    /// </summary>
    private IAssignmentService AssignmentService {
      get {
        return new AssignmentService(
          new GraphService(new ArrayService(33)), 
          new ApplicantFactory(new CodeOnceGeoService(new PdokService(), new NonPersistantGeoStorage())));
      }
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tests for handling of double ID.
    /// </summary>
    [Test]
    public void AssignmentDoubleIdTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Aanvragen));
      workbook.SetCellValue("Aanvragen", 30, 0, "29");
      string message;
      Assert.IsFalse(this.AssignmentService.TryAddAssignment(workbook, out message));
      Assert.IsTrue(message.Contains("kenmerk"));
      Assert.IsTrue(message.Contains("Dubbele"));
    }

    /// <summary>
    /// Tests for handling of missing ID.
    /// </summary>
    [Test]
    public void AssignmentMissingIdTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Aanvragen));
      string message;
      Assert.IsFalse(this.AssignmentService.TryAddAssignment(workbook, out message));
      Assert.IsTrue(message.Contains("kenmerk"));
      Assert.IsFalse(message.Contains("dubbel"));
    }

    /// <summary>
    /// Tests for handling of happy path.
    /// </summary>
    [Test]
    [Explicit]
    public void AssignmentHappyPathTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Aanvragen));
      workbook.SetCellValue("Aanvragen", 30, 0, "30");
      string message;
      var ok = this.AssignmentService.TryAddAssignment(workbook, out message);
      Console.WriteLine(message);
      Assert.IsTrue(ok);
      File.WriteAllBytes("toegewezen.xlsx", workbook.Data.ToArray());
    }

    /// <summary>
    /// Test to determine reciprocity frequency.
    /// </summary>
    [Test]
    [Explicit]
    public void AssignmentReciprocityTest() {
      var score = new int[1000];
      var source = File.ReadAllBytes(@"..\..\..\Oblaka.PeerReview.Web\Content\demo-toewijzen.xlsx");
      var assignmentService = this.AssignmentService;
      foreach (var i in Enumerable.Range(0, score.Length)) {
        var workbook = this.ExcelFactory.Read(new MemoryStream(source));
        string message;
        var ok = assignmentService.TryAddAssignment(workbook, out message);
        Assert.IsTrue(ok);
        var seen = new HashSet<Tuple<string, string>>();
        foreach (var rownum in Enumerable.Range(1, 30)) {
          var reviewer = workbook.GetCellValue("Toewijzingen", rownum, 0);
          foreach (var colnum in Enumerable.Range(2, 5)) {
            var aanvrager = workbook.GetCellValue("Toewijzingen", rownum, colnum);
            seen.Add(new Tuple<string, string>(reviewer, aanvrager));
            if (seen.Contains(new Tuple<string, string>(aanvrager, reviewer))) {
              score[i]++;
            }
          }
        }

        Assert.AreEqual(0, score[i], "Reciprocity should not occur in iteration " + i);
      }
    }

    #endregion
  }
}