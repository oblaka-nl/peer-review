﻿namespace Oblaka.PeerReview.Services.Tests {
  using System;
  using System.Linq;

  using NUnit.Framework;

  /// <summary>
  /// The ArrayService tests.
  /// </summary>
  [TestFixture]
  public class ArrayServiceTests {
    #region Public Methods and Operators

    /// <summary>
    /// Tests if shuffle results in an array with the same elements as the source array.
    /// </summary>
    [Test]
    public void ShuffleResultContainsSameElements() {
      var rng = new Random(33);
      foreach (var t in Enumerable.Range(0, 10)) {
        var source = (from i in Enumerable.Range(0, 50) select rng.Next(20)).ToArray();
        var sorted = source.OrderBy(i => i).ToArray();
        new ArrayService(rng.Next()).Shuffle(source);
        Assert.IsTrue(source.OrderBy(i => i).SequenceEqual(sorted));
      }
    }

    /// <summary>
    /// Tests if shuffle results are equally likely.
    /// </summary>
    [Test]
    public void ShuffleResultsAreEquallyLikely() {
      var rng = new Random(33);
      int[][] outcomes = {
                           new[] { 0, 1, 2 }, new[] { 0, 2, 1 }, new[] { 1, 0, 2 }, new[] { 1, 2, 0 }, new[] { 2, 0, 1 }, 
                           new[] { 2, 1, 0 }
                         };
      var counters = new int[outcomes.Length];
      foreach (var i in Enumerable.Range(0, 15000)) {
        var array = new[] { 0, 1, 2 };
        new ArrayService(rng.Next()).Shuffle(array);
        counters[Array.FindIndex(outcomes, array.SequenceEqual)]++;
      }

      Assert.IsTrue(counters.All(counter => counter >= 2400 && counter <= 2600));
    }

    #endregion
  }
}