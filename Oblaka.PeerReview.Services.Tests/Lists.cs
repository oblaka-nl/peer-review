﻿namespace Oblaka.PeerReview.Services.Tests {
  using System;

  /// <summary>
  /// Test lists.
  /// </summary>
  internal static class Lists {
    #region Static Fields

    /// <summary>
    /// Given names.
    /// </summary>
    public static readonly string[] GivenNames = Resources.Namen.Split(
      "\r\n".ToCharArray(), 
      StringSplitOptions.RemoveEmptyEntries);

    /// <summary>
    /// Localities in Brabant.
    /// </summary>
    public static readonly string[] LocalitiesBrabant = Resources.Brabant.Split(
      "\r\n".ToCharArray(), 
      StringSplitOptions.RemoveEmptyEntries);

    #endregion
  }
}