﻿namespace Oblaka.PeerReview.Services.Tests {
  using System;

  using NUnit.Framework;

  /// <summary>
  /// The GeoService tests.
  /// </summary>
  public abstract class GeoServiceTests {
    #region Properties

    /// <summary>
    /// Gets the geo service.
    /// </summary>
    protected abstract IGeoService GeoService { get; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tests all localities in Brabant.
    /// </summary>
    [Test]
    public void TestBrabant() {
      foreach (var locality in Lists.LocalitiesBrabant) {
        this.AssertInBrabant(locality, this.GeoService.CodeByLocality(locality));
      }
    }

    /// <summary>
    /// Tests the locality Bussum.
    /// </summary>
    /// <remarks>Locality does not occur in Noord-Brabant.</remarks>
    [Test]
    public void TestLocalityBussum() {
      AssertPoint(139836.0947457555M, 475819.12361925724M, this.GeoService.CodeByLocality("Bussum"));
    }

    /// <summary>
    /// Tests the locality Chaam.
    /// </summary>
    /// <remarks>Locality is part of a municipality.</remarks>
    [Test]
    public void TestLocalityChaam() {
      AssertPoint(118731.80927380869M, 390813.3089247075M, this.GeoService.CodeByLocality("Chaam"));
    }

    /// <summary>
    /// Tests the locality Eindhoven.
    /// </summary>
    /// <remarks>Locality and municipality have the same name.</remarks>
    [Test]
    public void TestLocalityEindhoven() {
      AssertPoint(159983.36614779354M, 384551.43825562194M, this.GeoService.CodeByLocality("Eindhoven"));
    }

    /// <summary>
    /// Tests the locality Vianen.
    /// </summary>
    /// <remarks>Locality occurs in various provincies, Noord-Brabant is preferred.</remarks>
    [Test]
    public void TestLocalityVianen() {
      AssertPoint(187255.0596662371M, 414417.0416186743M, this.GeoService.CodeByLocality("Vianen"));
    }

    /// <summary>
    /// Tests the postal code 1401AB.
    /// </summary>
    /// <remarks>Postal code is not in Noord-Brabant.</remarks>
    [Test]
    public void TestPostalCode1401AB() {
      AssertPoint(140091.048M, 477061.201M, this.GeoService.CodeByPostalCode("1401AB"));
    }

    /// <summary>
    /// Tests the postal code 5616 HZ.
    /// </summary>
    /// <remarks>Postal code contains space.</remarks>
    [Test]
    public void TestPostalCode5616HZ() {
      AssertPoint(160038.483M, 383120.226M, this.GeoService.CodeByPostalCode("5616 HZ"));
    }

    /// <summary>
    /// Tests the postal code Chaam.
    /// </summary>
    /// <remarks>Postal code is not a postal code.</remarks>
    [Test]
    public void TestPostalCodeChaam() {
      Assert.IsNull(this.GeoService.CodeByPostalCode("Chaam"));
    }

    #endregion

    #region Methods

    /// <summary>
    /// Performs asserts on point.
    /// </summary>
    /// <param name="x">
    /// The expected x.
    /// </param>
    /// <param name="y">
    /// The expected y.
    /// </param>
    /// <param name="point">
    /// The actual point.
    /// </param>
    private static void AssertPoint(decimal x, decimal y, IPoint point) {
      Assert.AreEqual(x, point.X);
      Assert.AreEqual(y, point.Y);
    }

    /// <summary>
    /// Asserts that point is in Brabant.
    /// </summary>
    /// <param name="locality">
    /// The locality.
    /// </param>
    /// <param name="point">
    /// The point.
    /// </param>
    private void AssertInBrabant(string locality, IPoint point) {
      Assert.IsNotNull(point, "Should have been found: {0}", locality);
      Assert.Greater(point.X, 7E4M, "Should be sufficiently east: {0}", locality);
      Assert.Less(point.X, 2E5M, "Should be sufficiently west: {0}", locality);
      Assert.Greater(point.Y, 36E4M, "Should be sufficiently north: {0}", locality);
      Assert.Less(point.Y, 43E4M, "Should be sufficiently south: {0}", locality);
    }

    #endregion
  }
}