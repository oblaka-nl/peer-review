﻿namespace Oblaka.PeerReview.Services.Tests.Excel {
  using System.IO;

  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Excel;

  /// <summary>
  /// The excel workbook tests.
  /// </summary>
  [TestFixture]
  public class ExcelWorkbookTests {
    #region Public Methods and Operators

    /// <summary>
    /// The add sheet test.
    /// </summary>
    [Test]
    public void AddSheetTest() {
      var workbook = new ExcelFactory().Read(new MemoryStream(ExcelWorkbooks.Aanvragen));
      var columns = new[] { "Kenmerk", "Postcode", "Plaats" };
      var data = workbook.ReadSheet("Aanvragen", columns);
      var name = workbook.AddSheet("Toewijzingen", columns, data);
      Assert.AreEqual("Toewijzingen-1", name);
    }

    /// <summary>
    /// The read sheet test.
    /// </summary>
    [Test]
    public void ReadSheetTest() {
      var workbook = new ExcelFactory().Read(new MemoryStream(ExcelWorkbooks.Aanvragen));
      var columns = new[] { "Kenmerk", "Postcode", "Plaats" };
      var data = workbook.ReadSheet("Aanvragen", columns);
      Assert.AreEqual(30, data.GetLength(0));
      Assert.AreEqual(3, data.GetLength(1));
      Assert.AreEqual("1", data[0, 0]);
      Assert.AreEqual("2", data[1, 0]);
      Assert.AreEqual("Oss", data[0, 2]);
    }

    /// <summary>
    /// The write sheet test.
    /// </summary>
    [Test]
    [Explicit]
    public void WriteSheetTest() {
      var workbook = new ExcelFactory().Read(new MemoryStream(ExcelWorkbooks.Aanvragen));
      var columns = new[] { "Kenmerk", "Postcode", "Plaats" };
      var data = workbook.ReadSheet("Aanvragen", columns);
      var name = workbook.AddSheet("Toewijzingen", columns, data);
      Assert.AreEqual("Toewijzingen-1", name);
      File.WriteAllBytes("toewijzingen.xlsx", workbook.Data.ToArray());
    }

    #endregion
  }
}