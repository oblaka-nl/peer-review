﻿namespace Oblaka.PeerReview.Services.Tests {
  using System;
  using System.IO;

  using NUnit.Framework;

  using Oblaka.PeerReview.Services.Excel;
  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The score sheet service tests.
  /// </summary>
  [TestFixture]
  public class ScoreSheetServiceTests {
    #region Properties

    /// <summary>
    /// Gets the excel factory.
    /// </summary>
    private IExcelFactory ExcelFactory {
      get {
        return new ExcelFactory();
      }
    }

    /// <summary>
    /// Gets the scoresheet service.
    /// </summary>
    private IScoreSheetService ScoreSheetService {
      get {
        return new ScoreSheetService();
      }
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// The score sheet double reviewer test.
    /// </summary>
    [Test]
    public void ScoreSheetDoubleReviewerTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      workbook.SetCellValue("Reviews", 5, 0, workbook.GetCellValue("Reviews", 4, 0));
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
#if EXPLICIT
      Console.WriteLine(message);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
#endif
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet double reviewed test.
    /// </summary>
    [Test]
    public void ScoreSheetDoubleReviewedTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      workbook.SetCellValue("Reviews", 5, 3, workbook.GetCellValue("Reviews", 5, 2));
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
#if EXPLICIT
      Console.WriteLine(message);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
#endif
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet null reviewed test.
    /// </summary>
    [Test]
    public void ScoreSheetNullReviewedTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      workbook.SetCellValue("Reviews", 5, 2, null);
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
#if EXPLICIT
      Console.WriteLine(message);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
#endif
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet null reviewer test.
    /// </summary>
    [Test]
    public void ScoreSheetNullReviewerTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      workbook.SetCellValue("Reviews", 5, 0, null);
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
#if EXPLICIT
      Console.WriteLine(message);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
#endif
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet invalid reviewer test.
    /// </summary>
    [Test]
    public void ScoreSheetInvalidReviewerTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      workbook.SetCellValue("Reviews", 5, 0, "foo");
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
#if EXPLICIT
      Console.WriteLine(message);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
#endif
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet invalid pin test.
    /// </summary>
    [Test]
    public void ScoreSheetInvalidPinTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      workbook.SetCellValue("Reviews", 5, 1, "yo");
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
#if EXPLICIT
      Console.WriteLine(message);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
#endif
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet missing cells test.
    /// </summary>
    [Test]
    public void ScoreSheetEmptyCellsTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      workbook.SetCellValue("Reviews", 5, 0, null);
      workbook.SetCellValue("Reviews", 6, 3, null);
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
#if EXPLICIT
      Console.WriteLine(message);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
#endif
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet happy path test.
    /// </summary>
    [Test]
    public void ScoreSheetHappyPathTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
      Assert.IsTrue(ok);
    }

    /// <summary>
    /// The score sheet happy path with save test.
    /// </summary>
    [Test]
    [Explicit]
    public void ScoreSheetHappyPathWithSaveTest() {
      var workbook = this.ExcelFactory.Read(new MemoryStream(Excel.ExcelWorkbooks.Reviews));
      string message;
      var ok = this.ScoreSheetService.TryAddScoreSheet(workbook, out message);
      Console.WriteLine(message);
      Assert.IsTrue(ok);
      File.WriteAllBytes("ranglijst.xlsx", workbook.Data.ToArray());
    }

    #endregion
  }
}