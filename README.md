Peer Review
===========

Oplossing voor het onderling eerlijk beoordelen van subsidie-aanvragen. Deze applicatie draait op http://peer-review.azurewebsites.net.

Installatie
-----------

Deze sectie beschrijft hoe de Peer Review applicatie zelf gehost kan worden.

###Benodigde onderdelen

* Microsoft Windows Server 2008 of hoger
* Microsoft Internet Information Server 7 of hoger
* Microsoft ASP.NET 4.5
* De binaire distributie van de Peer Review applicatie (https://bitbucket.org/oblaka-nl/peer-review/downloads/peer-review.zip)

###Installatiestappen

* Maak een directory peer-review.
* Pak de Peer Review zip uit in de map peer-review (dwz: bestanden komen direct in deze map).
* Maak in IIS een website met als root directory de map peer-review.  
  Zorg dat de application pool die hoort bij deze website onder .NET Framework 4.0 draait.

Unlicense
---------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

The software is provided "as is", without warranty of any kind,
express or implied, including but not limited to the warranties of
merchantability, fitness for a particular purpose and noninfringement.
In no event shall the authors be liable for any claim, damages or
other liability, whether in an action of contract, tort or otherwise,
arising from, out of or in connection with the software or the use or
other dealings in the software.

For more information, please refer to <http://unlicense.org/>