﻿namespace Oblaka.PeerReview.Services {
  using System.IO;

  /// <summary>
  /// The ExcelWorkbook interface.
  /// </summary>
  public interface IExcelWorkbook {
    #region Public Properties

    /// <summary>
    /// Gets the workbook data.
    /// </summary>
    MemoryStream Data { get; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Adds a sheet.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="columnNames">
    /// The column names.
    /// </param>
    /// <param name="data">
    /// The sheet data.
    /// </param>
    /// <returns>
    /// The actual (undoubled) sheet name.
    /// </returns>
    string AddSheet(string sheetName, string[] columnNames, string[,] data);

    /// <summary>
    /// Gets the cell value.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="rownum">
    /// The rownum.
    /// </param>
    /// <param name="colnum">
    /// The colnum.
    /// </param>
    /// <returns>
    /// The <see cref="string"/>.
    /// </returns>
    string GetCellValue(string sheetName, int rownum, int colnum);

    /// <summary>
    /// Reads the named columns from the named sheet.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="columnNames">
    /// The column names.
    /// </param>
    /// <returns>
    /// The data values.
    /// </returns>
    string[,] ReadSheet(string sheetName, string[] columnNames);

    /// <summary>
    /// Sets the cell value.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="rownum">
    /// The rownum.
    /// </param>
    /// <param name="colnum">
    /// The colnum.
    /// </param>
    /// <param name="value">
    /// The value.
    /// </param>
    void SetCellValue(string sheetName, int rownum, int colnum, string value);

    #endregion
  }
}