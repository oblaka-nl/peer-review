namespace Oblaka.PeerReview.Services {
  using System;
  using System.Xml.Serialization;

  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The Point interface.
  /// </summary>
  public interface IPoint {
    #region Public Properties

    /// <summary>
    /// Gets the x coordinate.
    /// </summary>
    decimal X { get; }

    /// <summary>
    /// Gets the y coordinate.
    /// </summary>
    decimal Y { get; }

    /// <summary>
    /// Gets or sets the position.
    /// </summary>
    [XmlElement(ElementName = "pos", Namespace = GeocodeResponse.GmlNamespace)]
    string Position { get; set; }

    #endregion
  }

  /// <summary>
  /// The point extensions.
  /// </summary>
  public static class PointExtensions {
    #region Public Methods and Operators

    /// <summary>
    /// Calculates the distance between two points.
    /// </summary>
    /// <param name="one">
    /// The one point.
    /// </param>
    /// <param name="other">
    /// The other point.
    /// </param>
    /// <returns>
    /// The distance between the points.
    /// </returns>
    public static decimal DistanceTo(this IPoint one, IPoint other) {
      return (decimal)Math.Sqrt(Sqr(one.X - other.X) + Sqr(one.Y - other.Y));
    }

    #endregion

    #region Methods

    /// <summary>
    /// Calculates the square of a number.
    /// </summary>
    /// <param name="x">
    /// Some decimal number.
    /// </param>
    /// <returns>
    /// The square of the numer.
    /// </returns>
    private static double Sqr(decimal x) {
      return (double)(x * x);
    }

    #endregion
  }
}