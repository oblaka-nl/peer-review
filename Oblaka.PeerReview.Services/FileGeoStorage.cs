﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;

  /// <summary>
  /// The file geo storage.
  /// </summary>
  public class FileGeoStorage : IGeoStorage {
    #region Fields

    /// <summary>
    /// The storage path.
    /// </summary>
    private readonly string path;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="FileGeoStorage"/> class.
    /// </summary>
    /// <param name="path">
    /// The storage path.
    /// </param>
    public FileGeoStorage(string path = "GeoStorage.txt") {
      this.path = path;
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Loads previously stored geocode results.
    /// </summary>
    /// <returns>
    /// The previously stored geocode results.
    /// </returns>
    public IEnumerable<Tuple<GeoType, string, IPoint>> Load() {
      return File.Exists(this.path)
               ? from line in File.ReadAllLines(this.path)
                 let parts = line.Split('&').Select(Uri.UnescapeDataString).ToArray()
                 where parts.Length == 3
                 select
                   new Tuple<GeoType, string, IPoint>(
                   (GeoType)Enum.Parse(typeof(GeoType), parts[0]), 
                   parts[1], 
                   string.IsNullOrEmpty(parts[2]) ? null : new Point { Position = parts[2] })
               : Enumerable.Empty<Tuple<GeoType, string, IPoint>>();
    }

    /// <summary>
    /// Saves a geocode result.
    /// </summary>
    /// <param name="geoType">
    /// The geo type.
    /// </param>
    /// <param name="argument">
    /// The argument.
    /// </param>
    /// <param name="point">
    /// The point.
    /// </param>
    public void Save(GeoType geoType, string argument, IPoint point) {
      string[] parts = { geoType.ToString(), argument, point == null ? string.Empty : point.Position };
      File.AppendAllLines(this.path, new[] { string.Join("&", parts.Select(Uri.EscapeDataString)) });
    }

    #endregion
  }
}