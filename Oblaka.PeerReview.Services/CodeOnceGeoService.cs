﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Collections.Generic;
  using System.Linq;

  /// <summary>
  /// The code once geo service.
  /// </summary>
  public class CodeOnceGeoService : IGeoService {
    #region Fields

    /// <summary>
    /// The cache.
    /// </summary>
    private readonly Dictionary<Tuple<GeoType, string>, IPoint> cache;

    /// <summary>
    /// The geo service.
    /// </summary>
    private readonly IGeoService geoService;

    /// <summary>
    /// The geo storage.
    /// </summary>
    private readonly IGeoStorage geoStorage;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="CodeOnceGeoService"/> class.
    /// </summary>
    /// <param name="geoService">
    /// The geo service.
    /// </param>
    /// <param name="geoStorage">
    /// The geo Storage.
    /// </param>
    public CodeOnceGeoService(IGeoService geoService, IGeoStorage geoStorage) {
      this.geoService = geoService;
      this.geoStorage = geoStorage;
      this.cache = this.geoStorage.Load().ToDictionary(t => new Tuple<GeoType, string>(t.Item1, t.Item2), t => t.Item3);
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// The code by locality.
    /// </summary>
    /// <param name="locality">
    /// The locality.
    /// </param>
    /// <returns>
    /// The <see cref="Point"/>.
    /// </returns>
    public IPoint CodeByLocality(string locality) {
      var key = new Tuple<GeoType, string>(GeoType.Locality, locality);
      IPoint point;
      if (this.cache.TryGetValue(key, out point)) {
        return point;
      }

      try {
        return this.cache[key] = point = this.geoService.CodeByLocality(locality);
      }
      finally {
        this.geoStorage.Save(GeoType.Locality, locality, point);
      }
    }

    /// <summary>
    /// The code by postal code.
    /// </summary>
    /// <param name="postalCode">
    /// The postal code.
    /// </param>
    /// <returns>
    /// The <see cref="Point"/>.
    /// </returns>
    public IPoint CodeByPostalCode(string postalCode) {
      var key = new Tuple<GeoType, string>(GeoType.PostalCode, postalCode);
      IPoint point;
      if (this.cache.TryGetValue(key, out point)) {
        return point;
      }

      try {
        return this.cache[key] = point = this.geoService.CodeByPostalCode(postalCode);
      }
      finally {
        this.geoStorage.Save(GeoType.PostalCode, postalCode, point);
      }
    }

    #endregion
  }
}