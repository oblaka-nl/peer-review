namespace Oblaka.PeerReview.Services {
  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The GeoService interface.
  /// </summary>
  public interface IGeoService {
    #region Public Methods and Operators

    /// <summary>
    /// The code by locality.
    /// </summary>
    /// <param name="locality">
    /// The locality.
    /// </param>
    /// <returns>
    /// The <see cref="Point"/>.
    /// </returns>
    IPoint CodeByLocality(string locality);

    /// <summary>
    /// The code by postal code.
    /// </summary>
    /// <param name="postalCode">
    /// The postal code.
    /// </param>
    /// <returns>
    /// The <see cref="Point"/>.
    /// </returns>
    IPoint CodeByPostalCode(string postalCode);

    #endregion
  }
}