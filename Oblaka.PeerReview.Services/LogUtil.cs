﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Globalization;
  using System.Threading;
  using System.Web;

  using NLog;

  /// <summary>
  /// The transaction event type.
  /// </summary>
  public enum TransactionalEventType {
    /// <summary>
    /// The complete.
    /// </summary>
    Complete, 

    /// <summary>
    /// The schedule.
    /// </summary>
    Schedule, 

    /// <summary>
    /// The assign.
    /// </summary>
    Assign, 

    /// <summary>
    /// The reassign.
    /// </summary>
    Reassign, 

    /// <summary>
    /// The start.
    /// </summary>
    Start, 

    /// <summary>
    /// The suspend.
    /// </summary>
    Suspend, 

    /// <summary>
    /// The resume.
    /// </summary>
    Resume, 

    /// <summary>
    /// The auto skip.
    /// </summary>
    AutoSkip, 

    /// <summary>
    /// The manual skip.
    /// </summary>
    ManualSkip, 

    /// <summary>
    /// The withdraw.
    /// </summary>
    Withdraw, 

    /// <summary>
    /// The abort activity.
    /// </summary>
    AbortActivity, 

    /// <summary>
    /// The abort case.
    /// </summary>
    AbortCase, 
  }

  /// <summary>
  /// The log util.
  /// </summary>
  public static class LogUtil {
    #region Public Methods and Operators

    /// <summary>
    /// Debugs an activity.
    /// </summary>
    /// <param name="source">
    /// The log source.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="transactionalEventType">
    /// The transactional event type.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    public static void Debug(
      this object source, 
      string activity, 
      TransactionalEventType transactionalEventType = TransactionalEventType.Complete, 
      object properties = null) {
      source.Log(LogLevel.Debug, activity, transactionalEventType, properties);
    }

    /// <summary>
    /// Errors an activity.
    /// </summary>
    /// <param name="source">
    /// The log source.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="transactionalEventType">
    /// The transactional event type.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    /// <param name="exception">
    /// The exception.
    /// </param>
    public static void Error(
      this object source, 
      string activity, 
      TransactionalEventType transactionalEventType = TransactionalEventType.Complete, 
      object properties = null, 
      Exception exception = null) {
      source.Log(LogLevel.Error, activity, transactionalEventType, properties, exception);
    }

    /// <summary>
    /// Fatals an activity.
    /// </summary>
    /// <param name="source">
    /// The log source.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="transactionalEventType">
    /// The transactional event type.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    /// <param name="exception">
    /// The exception.
    /// </param>
    public static void Fatal(
      this object source, 
      string activity, 
      TransactionalEventType transactionalEventType = TransactionalEventType.Complete, 
      object properties = null, 
      Exception exception = null) {
      source.Log(LogLevel.Fatal, activity, transactionalEventType, properties, exception);
    }

    /// <summary>
    /// Infos an activity.
    /// </summary>
    /// <param name="source">
    /// The log source.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="transactionalEventType">
    /// The transactional event type.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    public static void Info(
      this object source, 
      string activity, 
      TransactionalEventType transactionalEventType = TransactionalEventType.Complete, 
      object properties = null) {
      source.Log(LogLevel.Info, activity, transactionalEventType, properties);
    }

    /// <summary>
    /// Logs an activity.
    /// </summary>
    /// <param name="source">
    /// The log source.
    /// </param>
    /// <param name="level">
    /// The log level.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="transactionalEventType">
    /// The transactional event type.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    /// <param name="exception">
    /// The exception.
    /// </param>
    public static void Log(
      this object source, 
      LogLevel level, 
      string activity, 
      TransactionalEventType transactionalEventType = TransactionalEventType.Complete, 
      object properties = null, 
      Exception exception = null) {
      var logEvent = new LogEventInfo(level, source.GetType().FullName, activity) { Exception = exception };

      logEvent.Properties["ActivityType"] = transactionalEventType.ToString();
      logEvent.Properties["ThreadId"] = Thread.CurrentThread.ManagedThreadId.ToString(CultureInfo.InvariantCulture);

      if (properties != null) {
        foreach (var prop in properties.GetType().GetProperties()) {
          logEvent.Properties[prop.Name] = prop.GetValue(properties);
        }
      }

      LogManager.GetLogger(logEvent.LoggerName).Log(logEvent);
    }

    /// <summary>
    /// Traces an activity.
    /// </summary>
    /// <param name="source">
    /// The log source.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="transactionalEventType">
    /// The transactional event type.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    public static void Trace(
      this object source, 
      string activity, 
      TransactionalEventType transactionalEventType = TransactionalEventType.Complete, 
      object properties = null) {
      source.Log(LogLevel.Trace, activity, transactionalEventType, properties);
    }

    /// <summary>
    /// Warns an activity.
    /// </summary>
    /// <param name="source">
    /// The log source.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="transactionalEventType">
    /// The transactional event type.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    public static void Warn(
      this object source, 
      string activity, 
      TransactionalEventType transactionalEventType = TransactionalEventType.Complete, 
      object properties = null) {
      source.Log(LogLevel.Warn, activity, transactionalEventType, properties);
    }

    /// <summary>
    /// Executes function with logging.
    /// </summary>
    /// <typeparam name="TResult">
    /// Type of result.
    /// </typeparam>
    /// <param name="source">
    /// The source.
    /// </param>
    /// <param name="func">
    /// The inner function.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    /// <param name="startLogLevel">
    /// The log level for start.
    /// </param>
    /// <param name="exceptionLogLevel">
    /// The log level for exception.
    /// </param>
    /// <param name="completeLogLevel">
    /// The log level for complete.
    /// </param>
    /// <param name="abortType">
    /// The transactional event type for abort.
    /// </param>
    /// <returns>
    /// The <see cref="TResult"/>.
    /// </returns>
    public static TResult ExecuteWithLog<TResult>(
      this object source, 
      Func<TResult> func, 
      string activity, 
      object properties = null, 
      LogLevel startLogLevel = null, 
      LogLevel exceptionLogLevel = null, 
      LogLevel completeLogLevel = null, 
      TransactionalEventType abortType = TransactionalEventType.AbortActivity) {
      TResult result = default(TResult);
      Action action = () => result = func();
      source.ExecuteWithLog(action, activity, properties, startLogLevel, exceptionLogLevel, completeLogLevel, abortType);
      return result;
    }

    /// <summary>
    /// Executes action with logging.
    /// </summary>
    /// <param name="source">
    /// The source.
    /// </param>
    /// <param name="action">
    /// The inner action.
    /// </param>
    /// <param name="activity">
    /// The activity.
    /// </param>
    /// <param name="properties">
    /// The properties.
    /// </param>
    /// <param name="startLogLevel">
    /// The log level for start.
    /// </param>
    /// <param name="exceptionLogLevel">
    /// The log level for exception.
    /// </param>
    /// <param name="completeLogLevel">
    /// The log level for complete.
    /// </param>
    /// <param name="abortType">
    /// The transactional event type for abort.
    /// </param>
    public static void ExecuteWithLog(
      this object source,
      Action action,
      string activity,
      object properties = null,
      LogLevel startLogLevel = null,
      LogLevel exceptionLogLevel = null,
      LogLevel completeLogLevel = null,
      TransactionalEventType abortType = TransactionalEventType.AbortActivity) {
      try {
        source.Log(startLogLevel ?? LogLevel.Trace, activity, TransactionalEventType.Start, properties);
        action();
        source.Log(completeLogLevel ?? LogLevel.Trace, activity, TransactionalEventType.Complete, properties);
      }
      catch (Exception exception) {
        source.Log(exceptionLogLevel ?? LogLevel.Error, activity, abortType, properties, exception);
        throw;
      }
    }

    #endregion
  }
}