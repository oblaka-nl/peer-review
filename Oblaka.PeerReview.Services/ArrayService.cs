﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Collections.Generic;
  using System.Diagnostics.CodeAnalysis;
  using System.Linq;

  /// <summary>
  /// The ArrayService interface.
  /// </summary>
  public interface IArrayService {
    #region Public Methods and Operators

    /// <summary>
    /// Gets a sample from the given array.
    /// </summary>
    /// <param name="array">
    /// The array.
    /// </param>
    /// <param name="count">
    /// The number of items in the sample.
    /// </param>
    /// <typeparam name="T">
    /// Type of items.
    /// </typeparam>
    /// <returns>
    /// The sample.
    /// </returns>
    T[] Sample<T>(T[] array, int count);

    /// <summary>
    /// Shuffles the given array.
    /// </summary>
    /// <param name="array">
    /// The array.
    /// </param>
    /// <typeparam name="T">
    /// The element type.
    /// </typeparam>
    /// <remarks>
    /// Implements Fisher-Yates shuffle
    /// </remarks>
    void Shuffle<T>(T[] array);

    #endregion
  }

  /// <summary>
  /// The array service extensions.
  /// </summary>
  public static class ArrayServiceExtensions {
    #region Public Methods and Operators

    /// <summary>
    /// Gets some random member.
    /// </summary>
    /// <param name="arrayService">
    /// The array service.
    /// </param>
    /// <param name="seq">
    /// The sequence.
    /// </param>
    /// <typeparam name="T">
    /// The type of item.
    /// </typeparam>
    /// <returns>
    /// The chosen item.
    /// </returns>
    public static T Some<T>(this IArrayService arrayService, IEnumerable<T> seq) {
      return arrayService.Sample(seq.ToArray(), 1).Single();
    }

    #endregion
  }

  /// <summary>
  /// The array service.
  /// </summary>
  [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed. Suppression is OK here.")]
  public class ArrayService : IArrayService {
    #region Fields

    /// <summary>
    /// The random number generator.
    /// </summary>
    private readonly Random rng;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="ArrayService"/> class.
    /// </summary>
    /// <param name="seed">
    /// The optional seed for the random number generator.
    /// </param>
    public ArrayService(int? seed = null) {
      this.rng = seed.HasValue ? new Random(seed.Value) : new Random();
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Gets a sample from the given array.
    /// </summary>
    /// <param name="array">
    /// The array.
    /// </param>
    /// <param name="count">
    /// The number of items in the sample.
    /// </param>
    /// <typeparam name="T">
    /// Type of items.
    /// </typeparam>
    /// <returns>
    /// The sample.
    /// </returns>
    public T[] Sample<T>(T[] array, int count) {
      if (array == null) {
        throw new ArgumentNullException("array");
      }

      if (count < 0 || count > array.Length) {
        throw new ArgumentException("Count must be non-negative and not larger than the length of the array", "count");
      }

      var taken = new List<int>();
      foreach (var i in Enumerable.Range(0, count)) {
        int n;
        do {
          n = this.rng.Next(array.Length);
        }
        while (taken.Contains(n));
        taken.Add(n);
      }

      return (from i in taken orderby i select array[i]).ToArray();
    }

    /// <summary>
    /// Shuffles the given array.
    /// </summary>
    /// <param name="array">
    /// The array.
    /// </param>
    /// <typeparam name="T">
    /// The element type.
    /// </typeparam>
    /// <remarks>
    /// Implements Fisher-Yates shuffle
    /// </remarks>
    public void Shuffle<T>(T[] array) {
      if (array == null) {
        throw new ArgumentNullException("array");
      }

      foreach (var i in Enumerable.Range(0, array.Length - 1)) {
        var j = i + this.rng.Next(array.Length - i);
        var t = array[j];
        array[j] = array[i];
        array[i] = t;
      }
    }

    #endregion
  }
}