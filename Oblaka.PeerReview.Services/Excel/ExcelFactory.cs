﻿namespace Oblaka.PeerReview.Services.Excel {
  using System.IO;

  /// <summary>
  /// The excel factory.
  /// </summary>
  public class ExcelFactory : IExcelFactory {
    #region Public Methods and Operators

    /// <summary>
    /// Reads excel workbook from stream.
    /// </summary>
    /// <param name="stream">
    /// The stream.
    /// </param>
    /// <returns>
    /// The <see cref="IExcelWorkbook"/>.
    /// </returns>
    public IExcelWorkbook Read(Stream stream) {
      return new ExcelWorkbook(stream);
    }

    #endregion
  }
}