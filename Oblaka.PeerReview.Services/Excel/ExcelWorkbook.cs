﻿namespace Oblaka.PeerReview.Services.Excel {
  using System;
  using System.Collections.Generic;
  using System.Globalization;
  using System.IO;
  using System.Linq;
  using NPOI.SS.UserModel;
  using NPOI.XSSF.UserModel;

  /// <summary>
  /// The excel workbook.
  /// </summary>
  public class ExcelWorkbook : IExcelWorkbook {
    #region Fields

    /// <summary>
    /// The data formatter.
    /// </summary>
    private readonly DataFormatter dataFormatter;

    /// <summary>
    /// The formula evaluator.
    /// </summary>
    private readonly IFormulaEvaluator formulaEvaluator;

    /// <summary>
    /// The workbook.
    /// </summary>
    private readonly IWorkbook workbook;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="ExcelWorkbook"/> class.
    /// </summary>
    /// <param name="workbookStream">
    /// The workbook stream.
    /// </param>
    public ExcelWorkbook(Stream workbookStream) {
      this.workbook = new XSSFWorkbook(workbookStream);
      this.formulaEvaluator = new XSSFFormulaEvaluator(this.workbook);
      this.dataFormatter = new DataFormatter(CultureInfo.InvariantCulture);
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets the workbook data.
    /// </summary>
    public MemoryStream Data {
      get {
        var temp = new MemoryStream();
        this.workbook.Write(temp);
        return new MemoryStream(temp.ToArray());
      }
    }

    #endregion

    #region Properties

    /// <summary>
    /// Gets all postfixes
    /// </summary>
    private static IEnumerable<string> Postfixes {
      get {
        return
          Enumerable.Repeat(string.Empty, 1)
            .Concat(
              Enumerable.Range(0, int.MaxValue).Select(i => string.Format(CultureInfo.InvariantCulture, "-{0}", i)));
      }
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Adds a sheet.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="columnNames">
    /// The column names.
    /// </param>
    /// <param name="data">
    /// The sheet data.
    /// </param>
    /// <returns>
    /// The actual (undoubled) sheet name.
    /// </returns>
    public string AddSheet(string sheetName, string[] columnNames, string[,] data) {
      var sheet =
        this.workbook.CreateSheet(
          Postfixes.Select(p => sheetName + p).First(name => this.workbook.GetSheet(name) == null));

      // Create cell style bold
      var font = this.workbook.CreateFont();
      font.Boldweight = (short)FontBoldWeight.Bold;
      var cellStyle = this.workbook.CreateCellStyle();
      cellStyle.SetFont(font);

      // Add header
      var header = sheet.CreateRow(0);
      foreach (var coli in Enumerable.Range(0, columnNames.Length)) {
        var cell = header.CreateCell(coli);
        cell.SetCellValue(columnNames[coli]);
        cell.CellStyle = cellStyle;
      }

      // Add data rows
      foreach (var rowi in Enumerable.Range(0, data.GetLength(0))) {
        var row = sheet.CreateRow(rowi + 1);
        foreach (var coli in Enumerable.Range(0, data.GetLength(1))) {
          Double parsed;
          if (!string.IsNullOrEmpty(data[rowi, coli]) && Double.TryParse(data[rowi, coli], out parsed)) {
            row.CreateCell(coli).SetCellValue(parsed);
          }
          else {
            row.CreateCell(coli).SetCellValue(data[rowi, coli]);
          }
        }
      }

      return sheet.SheetName;
    }

    /// <summary>
    /// Gets the cell value.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="rownum">
    /// The rownum.
    /// </param>
    /// <param name="colnum">
    /// The colnum.
    /// </param>
    /// <returns>
    /// The <see cref="string"/>.
    /// </returns>
    public string GetCellValue(string sheetName, int rownum, int colnum) {
      var sheet = this.workbook.GetSheet(sheetName);
      if (sheet == null) {
        return null;
      }

      var row = sheet.GetRow(rownum);
      return row == null ? null : this.RenderCell(row.GetCell(colnum));
    }

    /// <summary>
    /// Sets the cell value.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="rownum">
    /// The rownum.
    /// </param>
    /// <param name="colnum">
    /// The colnum.
    /// </param>
    /// <param name="value">
    /// The value.
    /// </param>
    public void SetCellValue(string sheetName, int rownum, int colnum, string value) {
      var sheet = this.workbook.GetSheet(sheetName) ?? this.workbook.CreateSheet(sheetName);
      var row = sheet.GetRow(rownum) ?? sheet.CreateRow(rownum);
      var cell = row.GetCell(colnum) ?? row.CreateCell(colnum);
      cell.SetCellValue(value);
    }

    /// <summary>
    /// Reads the named columns from the named sheet.
    /// </summary>
    /// <param name="sheetName">
    /// The sheet name.
    /// </param>
    /// <param name="columnNames">
    /// The column names.
    /// </param>
    /// <returns>
    /// The data values.
    /// </returns>
    public string[,] ReadSheet(string sheetName, string[] columnNames) {
      var sheet = Postfixes.Select(p => this.workbook.GetSheet(sheetName + p)).TakeWhile(s => s != null).LastOrDefault();
      if (sheet == null) {
        throw new InvalidDataException(string.Format("Het document bevat geen werkblad met de naam {0}.", sheetName));
      }

      var columnIndexes = (from columnName in columnNames
                           join header in this.GetColumnHeaders(sheet) on columnName equals header.Key into headers
                           select headers.DefaultIfEmpty(new KeyValuePair<string, int>(null, -1)).FirstOrDefault().Value)
        .ToArray();
      var missing =
        (from kvp in columnNames.Zip(columnIndexes, (name, index) => new KeyValuePair<string, int>(name, index))
         where kvp.Value < 0
         select kvp.Key).ToArray();
      if (missing.Any()) {
        throw new InvalidDataException(
          string.Format(
            "Werkblad {0} bevat niet alle vereiste kolommen; missende kolommen: {1}.",
            sheet.SheetName,
            string.Join(", ", missing)));
      }

      var rows = (from rownum in Enumerable.Range(sheet.FirstRowNum + 1, sheet.LastRowNum - sheet.FirstRowNum)
                  let row = sheet.GetRow(rownum)
                  where
                    row != null
                    && columnIndexes.Any(colnum => !string.IsNullOrWhiteSpace(this.RenderCell(row.GetCell(colnum))))
                  select row).ToArray();
      var result = new string[rows.Length, columnNames.Length];
      foreach (var rowi in Enumerable.Range(0, rows.Length)) {
        var row = rows[rowi];
        foreach (var coli in Enumerable.Range(0, columnIndexes.Length)) {
          result[rowi, coli] = this.RenderCell(row.GetCell(columnIndexes[coli]));
        }
      }

      return result;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Gets the column headers.
    /// </summary>
    /// <param name="sheet">
    /// The sheet.
    /// </param>
    /// <returns>
    /// The column headers and their positions.
    /// </returns>
    private IEnumerable<KeyValuePair<string, int>> GetColumnHeaders(ISheet sheet) {
      var row = sheet.GetRow(sheet.FirstRowNum);
      return row == null
               ? Enumerable.Empty<KeyValuePair<string, int>>()
               : from i in Enumerable.Range(0, row.LastCellNum + 1)
                 let name = this.RenderCell(row.GetCell(i))
                 where !string.IsNullOrWhiteSpace(name)
                 select new KeyValuePair<string, int>(name, i);
    }

    /// <summary>
    /// Renders the cell value to string.
    /// </summary>
    /// <param name="cell">
    /// The sheet cell.
    /// </param>
    /// <returns>
    /// The rendered <see cref="string"/>.
    /// </returns>
    private string RenderCell(ICell cell) {
      return cell == null ? null : this.dataFormatter.FormatCellValue(cell, this.formulaEvaluator);
    }

    #endregion
  }
}