﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Collections.Generic;

  /// <summary>
  /// The geo type.
  /// </summary>
  public enum GeoType {
    /// <summary>
    /// No type specified.
    /// </summary>
    None, 

    /// <summary>
    /// The locality.
    /// </summary>
    Locality, 

    /// <summary>
    /// The postal code.
    /// </summary>
    PostalCode
  }

  /// <summary>
  /// The GeoStorage interface.
  /// </summary>
  public interface IGeoStorage {
    #region Public Methods and Operators

    /// <summary>
    /// Loads previously stored geocode results.
    /// </summary>
    /// <returns>
    /// The previously stored geocode results.
    /// </returns>
    IEnumerable<Tuple<GeoType, string, IPoint>> Load();

    /// <summary>
    /// Saves a geocode result.
    /// </summary>
    /// <param name="geoType">
    /// The geo type.
    /// </param>
    /// <param name="argument">
    /// The argument.
    /// </param>
    /// <param name="point">
    /// The point.
    /// </param>
    void Save(GeoType geoType, string argument, IPoint point);

    #endregion
  }
}