﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Collections.Generic;
  using System.Globalization;
  using System.IO;
  using System.Linq;

  /// <summary>
  /// The ScoreSheetService interface.
  /// </summary>
  public interface IScoreSheetService {
    #region Public Methods and Operators

    /// <summary>
    /// Tries to add scoresheet to workbook.
    /// </summary>
    /// <param name="workbook">
    /// The workbook.
    /// </param>
    /// <param name="message">
    /// The error or success message.
    /// </param>
    /// <returns>
    /// Whether the operation was succesful.
    /// </returns>
    bool TryAddScoreSheet(IExcelWorkbook workbook, out string message);

    #endregion
  }

  /// <summary>
  /// The score sheet service.
  /// </summary>
  public class ScoreSheetService : IScoreSheetService {
    #region Static Fields

    /// <summary>
    /// The review columns.
    /// </summary>
    private static readonly string[] ReviewColumns = { "Reviewer", "PIN", "Eerste", "Tweede", "Derde", "Vierde", "Vijfde" };

    /// <summary>
    /// The score columns.
    /// </summary>
    private static readonly string[] ScoreColumns = { "Aanvrager", "Eerste", "Tweede", "Derde", "Vierde", "Vijfde", "Verstek", "Score", "Gereviewed" };

    /// <summary>
    /// The score values.
    /// </summary>
    private static readonly int[] ScoreValues = { 6, 4, 3, 2, 0 };

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tries to add scoresheet to workbook.
    /// </summary>
    /// <param name="workbook">
    /// The workbook.
    /// </param>
    /// <param name="message">
    /// The error or success message.
    /// </param>
    /// <returns>
    /// Whether the operation was succesful.
    /// </returns>
    public bool TryAddScoreSheet(IExcelWorkbook workbook, out string message) {
      string innerMessage = null;
      Func<bool> tryAddScoreSheet = () => {
        IDictionary<string, Tuple<string, string[]>> assignments;
        IDictionary<string, string[]> reviews;

        var warnings = new List<string>();

        try {
          assignments = this.ExecuteWithLog(
            () => this.ReadAssignments(workbook),
            "TryAddScoreSheet-ReadAssignments");
          reviews = this.ExecuteWithLog(
            () => this.ReadReviews(workbook, assignments, warnings),
            "TryAddScoreSheet-ReadReviews");
        }
        catch (InvalidDataException e) {
          innerMessage = e.Message;
          return false;
        }

        var placeGroups = from value in reviews.Values
                          from place in Enumerable.Range(0, value.Length)
                          group place by value[place]
                            into placeGroup
                            select placeGroup;
        var reviewed = from key in assignments.Keys
                       join placeGroup in placeGroups on key equals placeGroup.Key into joinedPlaces
                       let places = joinedPlaces.Cast<IEnumerable<int>>().FirstOrDefault() ?? Enumerable.Empty<int>()
                       let defaults = AssignmentService.ReviewCount - places.Count()
                       let record =
                         (new {
                           Key = key,
                           PerPlace = (from place in Enumerable.Range(0, AssignmentService.ReviewCount)
                                       join p in places on place equals p into matched
                                       select matched.Count()).ToArray(),
                           Defaults = defaults,
                           Score =
                    places.Concat(Enumerable.Repeat(AssignmentService.ReviewCount / 2, defaults))
                    .Sum(place => ScoreValues[place]),
                           Reviewed = reviews.ContainsKey(key)
                         })
                       orderby record.Score descending, record.Defaults, record.Reviewed descending, record.Key
                       select record;
        var scoreData = new string[assignments.Count, ScoreColumns.Length];
        foreach (
          var pair in
            reviewed.Zip(
              Enumerable.Range(0, assignments.Count),
              (record, rownum) => new {
                RowNum = rownum,
                Record = record
              })) {
          var rownum = pair.RowNum;
          var record = pair.Record;
          scoreData[rownum, 0] = record.Key;
          foreach (var place in Enumerable.Range(0, AssignmentService.ReviewCount)) {
            scoreData[rownum, 1 + place] = record.PerPlace[place].ToString(CultureInfo.InvariantCulture);
          }

          scoreData[rownum, AssignmentService.ReviewCount + 1] = record.Defaults.ToString(CultureInfo.InvariantCulture);
          scoreData[rownum, AssignmentService.ReviewCount + 2] = record.Score.ToString(CultureInfo.InvariantCulture);
          scoreData[rownum, AssignmentService.ReviewCount + 3] = record.Reviewed ? "Ja" : "Nee";
        }

        var counters = new {
          AssignmentCount = assignments.Count,
          ReviewCount = reviews.Count
        };
        this.ExecuteWithLog(
          () => workbook.AddSheet("Ranglijst", ScoreColumns, scoreData),
          "ScoreSheetService-AddRanglijst",
          counters);

        string warningSheet = null;
        if (warnings.Any()) {
          var warningData = new string[warnings.Count, 1];
          foreach (var w in Enumerable.Range(0, warnings.Count)) {
            warningData[w, 0] = warnings[w];
          }

          warningSheet = this.ExecuteWithLog(
            () => workbook.AddSheet("Meldingen", new[] { "Melding" }, warningData),
            "ScoreSheetService-AddMeldingen",
            counters);
        }

        innerMessage = "Ranglijst succesvol toegevoegd. "
                  + (warnings.Any()
                       ? string.Format("Zie het werkblad '{0}'.", warningSheet)
                       : "Er waren geen bijzonderheden.");
        return true;
      };
      try {
        return this.ExecuteWithLog(tryAddScoreSheet, "TryAddScoreSheet");
      }
      finally {
        message = innerMessage;
      }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Reads the assignments.
    /// </summary>
    /// <param name="workbook">
    /// The workbook.
    /// </param>
    /// <returns>
    /// The <see cref="IDictionary"/>.
    /// </returns>
    private IDictionary<string, Tuple<string, string[]>> ReadAssignments(IExcelWorkbook workbook) {
      var data = workbook.ReadSheet("Toewijzingen", AssignmentService.AssignmentColumns);
      if (data.GetLength(0) == 0) {
        throw new InvalidDataException("Er zijn geen toewijzingen.");
      }

      if ((from rownum in Enumerable.Range(0, data.GetLength(0))
           from colnum in Enumerable.Range(0, data.GetLength(1))
           where string.IsNullOrWhiteSpace(data[rownum, colnum])
           select colnum).Any()) {
        throw new InvalidDataException("Het tabblad Toewijzingen bevat lege waarden.");
      }

      if ((from rownum in Enumerable.Range(0, data.GetLength(0))
           group rownum by data[rownum, 0]
             into id
             where id.Count() > 1
             select id).Any()) {
        throw new InvalidDataException("Het tabblad Toewijzingen bevat dubbele waarden in de kolom Reviewer.");
      }

      return Enumerable.Range(0, data.GetLength(0))
        .ToDictionary(
          rownum => data[rownum, 0],
          rownum =>
          new Tuple<string, string[]>(
            data[rownum, 1],
            Enumerable.Range(0, data.GetLength(1)).Skip(2).Select(colnum => data[rownum, colnum]).ToArray()));
    }

    /// <summary>
    /// Reads the reviews.
    /// </summary>
    /// <param name="workbook">
    /// The workbook.
    /// </param>
    /// <param name="assignments">
    /// The assignments.
    /// </param>
    /// <param name="warnings">
    /// A list to append warnings to.
    /// </param>
    /// <returns>
    /// The <see cref="IDictionary"/>.
    /// </returns>
    private IDictionary<string, string[]> ReadReviews(IExcelWorkbook workbook, IDictionary<string, Tuple<string, string[]>> assignments, List<string> warnings) {
      var data = workbook.ReadSheet("Reviews", ReviewColumns);
      var reviews = new Dictionary<string, string[]>();
      foreach (var rownum in Enumerable.Range(0, data.GetLength(0))) {
        if (Enumerable.Range(0, data.GetLength(1)).Any(colnum => string.IsNullOrWhiteSpace(data[rownum, colnum]))) {
          warnings.Add(
            string.Format(
              "Cel {0}{1} in werkblad Reviews is leeg: rij wordt genegeerd.",
              "ABCDEFGHIJ"[Enumerable.Range(0, data.GetLength(1)).First(colnum => string.IsNullOrWhiteSpace(data[rownum, colnum]))],
              rownum + 2));
          continue;
        }

        if (reviews.ContainsKey(data[rownum, 0])) {
          warnings.Add(
            string.Format(
              "Rij {0} bevat extra review van reviewer '{1}': rij wordt genegeerd.",
              rownum + 2,
              data[rownum, 0]));
          continue;
        }

        Tuple<string, string[]> assigned;
        if (!assignments.TryGetValue(data[rownum, 0], out assigned)) {
          warnings.Add(
            string.Format(
              "Rij {0} bevat een onbekende reviewer '{1}': rij wordt genegeerd.",
              rownum + 2,
              data[rownum, 0]));
          continue;
        }

        if (data[rownum, 1] != assigned.Item1) {
          warnings.Add(
            string.Format(
              "Rij {0} bevat een onjuiste PIN voor reviewer '{1}': rij wordt genegeerd.",
              rownum + 2,
              data[rownum, 0]));
          continue;
        }

        var reviewed =
          (from colnum in Enumerable.Range(0, data.GetLength(1)).Skip(2)
           select data[rownum, colnum]).ToArray();
        if (assigned.Item2.Except(reviewed).Any()) {
          warnings.Add(
            string.Format(
              "Rij {0} van reviewer '{1}' bevat niet de toegewezen kenmerken: rij wordt genegeerd.",
              rownum + 2,
              data[rownum, 0]));
          continue;
        }

        reviews[data[rownum, 0]] = reviewed;
      }

      warnings.AddRange(
        from key in assignments.Keys.Except(reviews.Keys)
        select string.Format("Geen (geldige) review ontvangen van reviewer '{0}'", key));

      if (!reviews.Any()) {
        throw new InvalidDataException("Er zijn geen geldige reviews.");
      }

      return reviews;
    }

    #endregion
  }
}