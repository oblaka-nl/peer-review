﻿namespace Oblaka.PeerReview.Services.Pdok {
  using System.Xml.Serialization;

  /// <summary>
  /// The geocode response.
  /// </summary>
  [XmlRoot(ElementName = "GeocodeResponse", Namespace = GeocodeResponse.XlsNamespace)]
  public class GeocodeResponse {
    #region Constants

    /// <summary>
    /// The gml namespace.
    /// </summary>
    public const string GmlNamespace = "http://www.opengis.net/gml";

    /// <summary>
    /// The xls namespace.
    /// </summary>
    public const string XlsNamespace = "http://www.opengis.net/xls";

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets or sets the addresses.
    /// </summary>
    [XmlArray(ElementName = "GeocodeResponseList", Namespace = XlsNamespace)]
    [XmlArrayItem(ElementName = "GeocodedAddress", Namespace = XlsNamespace)]
    public GeocodedAddress[] Addresses { get; set; }

    #endregion
  }
}