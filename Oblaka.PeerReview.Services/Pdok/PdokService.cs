﻿namespace Oblaka.PeerReview.Services.Pdok {
  using System;
  using System.Linq;
  using System.Net;
  using System.Text.RegularExpressions;
  using System.Web;
  using System.Xml.Serialization;

  /// <summary>
  /// The pdok geo service.
  /// </summary>
  public class PdokService : IGeoService {
    #region Constants

    /// <summary>
    /// The pdok url.
    /// </summary>
    private const string PdokUrl = "http://geodata.nationaalgeoregister.nl/geocoder/Geocoder?zoekterm=";

    #endregion

    #region Static Fields

    /// <summary>
    /// The allowed place types for locality.
    /// </summary>
    private static readonly PlaceType[] AllowedPlaceTypesForLocality = {
                                                                         PlaceType.Municipality, 
                                                                         PlaceType.MunicipalitySubdivision
                                                                       };

    /// <summary>
    /// The geo serializer.
    /// </summary>
    private static readonly XmlSerializer GeoSerializer = new XmlSerializer(typeof(GeocodeResponse));

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// The code by locality.
    /// </summary>
    /// <param name="locality">
    /// The locality.
    /// </param>
    /// <returns>
    /// The <see cref="Point"/>.
    /// </returns>
    public IPoint CodeByLocality(string locality) {
      if (string.IsNullOrWhiteSpace(locality)) {
        return null;
      }

      Predicate<Address> validator =
        address => AllowedPlaceTypesForLocality.Contains(address.Places[0].Type) && address.Streets == null;
      return this.ExecuteWithLog(
        () => Search(locality, validator),
        "CodeByLocality",
        new { Locality = locality });
    }

    /// <summary>
    /// The code by postal code.
    /// </summary>
    /// <param name="postalCode">
    /// The postal code.
    /// </param>
    /// <returns>
    /// The <see cref="Point"/>.
    /// </returns>
    public IPoint CodeByPostalCode(string postalCode) {
      if (string.IsNullOrWhiteSpace(postalCode)) {
        return null;
      }

      postalCode = Regex.Replace(postalCode, @"\s+", string.Empty);
      Predicate<Address> validator =
        address => postalCode.Equals(address.PostalCode, StringComparison.InvariantCultureIgnoreCase);
      return this.ExecuteWithLog(
        () => Search(postalCode, validator),
        "CodeByPostalCode",
        new { PostalCode = postalCode });
    }

    #endregion

    #region Methods

    /// <summary>
    /// Searches in PDOK.
    /// </summary>
    /// <param name="search">
    /// The search term.
    /// </param>
    /// <param name="validator">
    /// The address validator.
    /// </param>
    /// <returns>
    /// The point of the most specific or otherwise first result or null.
    /// </returns>
    private static Point Search(string search, Predicate<Address> validator) {
      var browser = new WebClient();
      GeocodeResponse geo;
      using (var response = browser.OpenRead(PdokUrl + HttpUtility.UrlEncode(search))) {
        geo = (GeocodeResponse)GeoSerializer.Deserialize(response);
        return geo.Addresses == null
                 ? null
                 : (from geocoded in geo.Addresses
                    let address = geocoded.Address
                    where geocoded.Point != null && address != null && address.Places != null
                    orderby
                      address.Places.Any(p => p.Type == PlaceType.CountrySubdivision && p.Name == "Noord-Brabant")
                      descending, address.Places.Length descending
                    where validator(address)
                    select geocoded.Point).FirstOrDefault();
      }
    }

    #endregion
  }
}