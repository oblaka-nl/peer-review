﻿namespace Oblaka.PeerReview.Services.Pdok {
  using System.Xml.Serialization;

  /// <summary>
  /// The geocoded address.
  /// </summary>
  [XmlRoot(ElementName = "GeocodedAddress", Namespace = GeocodeResponse.XlsNamespace)]
  public class GeocodedAddress {
    #region Public Properties

    /// <summary>
    /// Gets or sets the address.
    /// </summary>
    [XmlElement(Order = 1, ElementName = "Address", Namespace = GeocodeResponse.XlsNamespace)]
    public Address Address { get; set; }

    /// <summary>
    /// Gets or sets the point.
    /// </summary>
    [XmlElement(Order = 0, ElementName = "Point", Namespace = GeocodeResponse.GmlNamespace)]
    public Point Point { get; set; }

    #endregion
  }
}