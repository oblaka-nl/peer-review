﻿namespace Oblaka.PeerReview.Services.Pdok {
  using System.Xml.Serialization;

  /// <summary>
  /// The place types.
  /// </summary>
  public enum PlaceType {
    /// <summary>
    /// The country subdivision.
    /// </summary>
    CountrySubdivision, 

    /// <summary>
    /// The country secondary subdivision.
    /// </summary>
    CountrySecondarySubdivision, 

    /// <summary>
    /// The municipality.
    /// </summary>
    Municipality, 

    /// <summary>
    /// The municipality subdivision.
    /// </summary>
    MunicipalitySubdivision
  }

  /// <summary>
  /// The place.
  /// </summary>
  [XmlRoot(ElementName = "Place", Namespace = GeocodeResponse.XlsNamespace)]
  public class Place {
    #region Public Properties

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    [XmlText]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the type.
    /// </summary>
    [XmlAttribute("type")]
    public PlaceType Type { get; set; }

    #endregion
  }
}