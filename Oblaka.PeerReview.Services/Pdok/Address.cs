﻿namespace Oblaka.PeerReview.Services.Pdok {
  using System.Xml.Serialization;

  /// <summary>
  /// The address.
  /// </summary>
  [XmlRoot(ElementName = "Address", Namespace = GeocodeResponse.XlsNamespace)]
  public class Address {
    #region Public Properties

    /// <summary>
    /// Gets or sets the places.
    /// </summary>
    [XmlElement(ElementName = "Place", Namespace = GeocodeResponse.XlsNamespace)]
    public Place[] Places { get; set; }

    /// <summary>
    /// Gets or sets the postal code.
    /// </summary>
    [XmlElement(ElementName = "PostalCode", Namespace = GeocodeResponse.XlsNamespace)]
    public string PostalCode { get; set; }

    /// <summary>
    /// Gets or sets the streets.
    /// </summary>
    [XmlArray(ElementName = "StreetAddress", Namespace = GeocodeResponse.XlsNamespace)]
    [XmlArrayItem(ElementName = "Street", Namespace = GeocodeResponse.XlsNamespace)]
    public string[] Streets { get; set; }

    #endregion
  }
}