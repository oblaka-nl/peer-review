﻿namespace Oblaka.PeerReview.Services {
  /// <summary>
  /// The ApplicantFactory interface.
  /// </summary>
  public interface IApplicantFactory {
    /// <summary>
    /// Initializes a new instance of the <see cref="Applicant"/> class.
    /// </summary>
    /// <param name="id">
    /// The applicant id.
    /// </param>
    /// <param name="postalCode">
    /// The postal code.
    /// </param>
    /// <param name="locality">
    /// The locality.
    /// </param>
    /// <returns>
    /// The <see cref="Applicant"/>.
    /// </returns>
    IApplicant CreateApplicant(string id, string postalCode, string locality);
  }

  /// <summary>
  /// The applicant factory.
  /// </summary>
  public class ApplicantFactory : IApplicantFactory {
    #region Fields

    /// <summary>
    /// The geo service.
    /// </summary>
    private readonly IGeoService geoService;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="ApplicantFactory"/> class.
    /// </summary>
    /// <param name="geoService">
    /// The geo service.
    /// </param>
    public ApplicantFactory(IGeoService geoService) {
      this.geoService = geoService;
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Initializes a new instance of the <see cref="Applicant"/> class.
    /// </summary>
    /// <param name="id">
    /// The applicant id.
    /// </param>
    /// <param name="postalCode">
    /// The postal code.
    /// </param>
    /// <param name="locality">
    /// The locality.
    /// </param>
    /// <returns>
    /// The <see cref="Applicant"/>.
    /// </returns>
    public IApplicant CreateApplicant(string id, string postalCode, string locality) {
      return new Applicant(id, postalCode, locality, this.geoService);
    }

    #endregion
  }
}