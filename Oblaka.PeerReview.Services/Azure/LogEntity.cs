namespace Oblaka.PeerReview.Services.Azure {
  using System;
  using System.Collections.Generic;
  using System.ComponentModel;
  using System.Globalization;
  using System.Linq;

  using Microsoft.WindowsAzure.Storage.Table;

  using NLog;

  /// <summary>
  /// The log entity.
  /// </summary>
  public class LogEntity : TableEntity {
    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="LogEntity"/> class.
    /// </summary>
    public LogEntity() {
      this.RowKey = Guid.NewGuid().ToString();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="LogEntity"/> class.
    /// </summary>
    /// <param name="info">
    /// The log event info.
    /// </param>
    public LogEntity(LogEventInfo info)
      : this() {
      if (info == null) {
        return;
      }

      this.PartitionKey = info.TimeStamp.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
      this.EventTimeStamp = info.TimeStamp;
      this.Level = info.Level;
      this.LoggerName = info.LoggerName;
      this.Message = info.FormattedMessage;
      this.SequenceId = info.SequenceID;

      if (info.Exception != null) {
        this.ExceptionMessage = info.Exception.Message;
        this.ExceptionStackTrace = info.Exception.StackTrace;
      }

      if (info.Properties != null) {
        this.Attributes = from prop in info.Properties
                          select
                            new KeyValuePair<string, string>(
                            Convert.ToString(prop.Key, CultureInfo.InvariantCulture), 
                            Convert.ToString(prop.Value, CultureInfo.InvariantCulture));
      }
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets or sets the attribute string.
    /// </summary>
    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public string AttributeString { get; set; }

    /// <summary>
    /// Gets or sets the attributes.
    /// </summary>
    public IEnumerable<KeyValuePair<string, string>> Attributes {
      get {
        return string.IsNullOrEmpty(this.AttributeString)
                 ? new Dictionary<string, string>()
                 : from part in this.AttributeString.Split('&')
                   let kvp = part.Split('=').Select(Uri.UnescapeDataString).ToArray()
                   select new KeyValuePair<string, string>(kvp[0], kvp[1]);
      }

      set {
        if (value == null) {
          this.AttributeString = null;
          return;
        }

        var list = value as IList<KeyValuePair<string, string>> ?? value.ToList();
        var parts = from kvp in list
                    where !string.IsNullOrEmpty(kvp.Key) && !string.IsNullOrEmpty(kvp.Value)
                    select string.Join("=", Uri.EscapeDataString(kvp.Key), Uri.EscapeDataString(kvp.Value));
        this.AttributeString = list.Count == 0 ? null : string.Join("&", parts);
      }
    }

    /// <summary>
    /// Gets or sets the event time stamp.
    /// </summary>
    public DateTime EventTimeStamp { get; set; }

    /// <summary>
    /// Gets or sets the exception message.
    /// </summary>
    public string ExceptionMessage { get; set; }

    /// <summary>
    /// Gets or sets the exception stack trace.
    /// </summary>
    public string ExceptionStackTrace { get; set; }

    /// <summary>
    /// Gets or sets the level.
    /// </summary>
    public LogLevel Level {
      get {
        return LogLevel.FromOrdinal(this.LevelOrdinal);
      }

      set {
        this.LevelOrdinal = value == null ? 0 : value.Ordinal;
      }
    }

    /// <summary>
    /// Gets or sets the level ordinal.
    /// </summary>
    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public int LevelOrdinal { get; set; }

    /// <summary>
    /// Gets or sets the logger name.
    /// </summary>
    public string LoggerName { get; set; }

    /// <summary>
    /// Gets or sets the message.
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// Gets or sets the sequence id.
    /// </summary>
    public int SequenceId { get; set; }

    #endregion
  }
}