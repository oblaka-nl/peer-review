﻿namespace Oblaka.PeerReview.Services.Azure {
  using System;
  using System.Collections.Generic;
  using System.Configuration;
  using System.Linq;

  using Microsoft.WindowsAzure.Storage;
  using Microsoft.WindowsAzure.Storage.Table;

  /// <summary>
  /// The azure geo storage.
  /// </summary>
  public class AzureGeoStorage : IGeoStorage {
    #region Fields

    /// <summary>
    /// The geocache.
    /// </summary>
    private readonly CloudTable geocache;

    /// <summary>
    /// The partition key.
    /// </summary>
    private readonly string partitionKey;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="AzureGeoStorage"/> class.
    /// </summary>
    /// <param name="connectionString">
    /// The connection string.
    /// </param>
    /// <param name="partitionKey">
    /// The partition key.
    /// </param>
    public AzureGeoStorage(string connectionString, string partitionKey = "peer-review") {
      if (connectionString == null) {
        throw new ArgumentNullException("connectionString");
      }

      var account = CloudStorageAccount.Parse(connectionString);
      this.partitionKey = partitionKey;
      var client = account.CreateCloudTableClient();
      this.geocache = client.GetTableReference("geocache");
      this.geocache.CreateIfNotExists();
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Loads previously stored geocode results.
    /// </summary>
    /// <returns>
    /// The previously stored geocode results.
    /// </returns>
    public IEnumerable<Tuple<GeoType, string, IPoint>> Load() {
      Func<IEnumerable<Tuple<GeoType, string, IPoint>>> load = () =>
        from item in
          this.geocache.ExecuteQuery(
            new TableQuery<GeoCodedItem>().Where(
              TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, this.partitionKey)))
        select
          new Tuple<GeoType, string, IPoint>(item.Type, item.Key, Point.FromPosition(item.Position));
      return this.ExecuteWithLog(load, "Load");
    }

    /// <summary>
    /// Saves a geocode result.
    /// </summary>
    /// <param name="geoType">
    /// The geo type.
    /// </param>
    /// <param name="argument">
    /// The argument.
    /// </param>
    /// <param name="point">
    /// The point.
    /// </param>
    public void Save(GeoType geoType, string argument, IPoint point) {
      Action save = () =>
        this.geocache.Execute(
          TableOperation.InsertOrReplace(
            new GeoCodedItem(geoType, argument, this.partitionKey) {
              Position = point == null ? null : point.Position
            }));
      var props = new {
        GeoType = geoType,
        Argument = argument
      };
      this.ExecuteWithLog(save, "Save", props);
    }

    /// <summary>
    /// The geo coded item.
    /// </summary>
    private class GeoCodedItem : TableEntity {
      #region Constructors and Destructors

      /// <summary>
      /// Initializes a new instance of the <see cref="GeoCodedItem"/> class.
      /// </summary>
      public GeoCodedItem() {
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="GeoCodedItem"/> class.
      /// </summary>
      /// <param name="type">
      /// The geo type.
      /// </param>
      /// <param name="key">
      /// The geo key.
      /// </param>
      /// <param name="partitionKey">
      /// The partition key.
      /// </param>
      public GeoCodedItem(GeoType type, string key, string partitionKey) {
        this.RowKey = string.Join("-", type, key);
        this.PartitionKey = partitionKey;
      }

      #endregion

      #region Public Properties

      /// <summary>
      /// Gets the key.
      /// </summary>
      public string Key {
        get {
          return string.IsNullOrEmpty(this.RowKey) ? null : this.RowKey.Substring(this.RowKey.IndexOf('-') + 1);
        }
      }

      /// <summary>
      /// Gets or sets the position.
      /// </summary>
      public string Position {
        get;
        set;
      }

      /// <summary>
      /// Gets the type.
      /// </summary>
      public GeoType Type {
        get {
          return string.IsNullOrEmpty(this.RowKey)
                   ? GeoType.None
                   : (GeoType)Enum.Parse(typeof(GeoType), this.RowKey.Split('-')[0]);
        }
      }

      #endregion
    }

    #endregion
  }
}