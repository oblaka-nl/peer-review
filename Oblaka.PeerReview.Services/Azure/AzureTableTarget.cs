﻿namespace Oblaka.PeerReview.Services.Azure {
  using Microsoft.WindowsAzure;
  using Microsoft.WindowsAzure.Storage;
  using Microsoft.WindowsAzure.Storage.Table;

  using NLog;
  using NLog.Config;
  using NLog.Targets;

  /// <summary>
  /// The azure table target.
  /// </summary>
  [Target("AzureTable")]
  public class AzureTableTarget : Target {
    #region Fields

    /// <summary>
    /// The eventlog.
    /// </summary>
    private CloudTable eventlog;

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets or sets the connection string alias.
    /// </summary>
    [RequiredParameter]
    public string ConnectionStringAlias { get; set; }

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets a value indicating whether target is enabled.
    /// </summary>
    private bool Enabled { get; set; }

    #endregion

    #region Methods

    /// <summary>
    /// Initializes the target. Can be used by inheriting classes
    /// to initialize logging.
    /// </summary>
    protected override void InitializeTarget() {
      base.InitializeTarget();
      var connectionString = CloudConfigurationManager.GetSetting(this.ConnectionStringAlias);
      this.Enabled = !string.IsNullOrWhiteSpace(connectionString);
      if (this.Enabled) {
        var account = CloudStorageAccount.Parse(connectionString);
        var client = account.CreateCloudTableClient();
        this.eventlog = client.GetTableReference("eventlog");
        this.eventlog.CreateIfNotExists();
      }
    }

    /// <summary>
    /// Writes logging event to the log target classes.
    /// </summary>
    /// <param name="logEvent">
    /// Logging event to be written out.
    /// </param>
    protected override void Write(LogEventInfo logEvent) {
      if (this.Enabled) {
        this.eventlog.Execute(TableOperation.Insert(new LogEntity(logEvent)));
      }
    }

    #endregion
  }
}