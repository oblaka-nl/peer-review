﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Collections.Generic;
  using System.Linq;

  using Microsoft.FSharp.Collections;
  using Microsoft.FSharp.Core;

  using NPOI.HSSF.Record;
  using NPOI.Util.Collections;

  using Oblaka.Discrete;

  /// <summary>
  /// The GraphService interface.
  /// </summary>
  public interface IGraphService {
    #region Public Properties

    /// <summary>
    /// Gets the array service.
    /// </summary>
    IArrayService ArrayService { get; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Gets asymmetric matchings.
    /// </summary>
    /// <param name="edges">
    /// The graph edges.
    /// </param>
    /// <param name="count">
    /// The number of matchings to make.
    /// </param>
    /// <returns>
    /// The matchings.
    /// </returns>
    int[,] GetAsymmetricMatchings(int[][] edges, int count);

    /// <summary>
    /// Gets non-reciprocical matchings.
    /// </summary>
    /// <param name="edges">
    /// The graph edges.
    /// </param>
    /// <param name="count">
    /// The number of matchings to make.
    /// </param>
    /// <returns>
    /// The matchings.
    /// </returns>
    int[,] GetNonReciprocicalMatchings(int[][] edges, int count);

    /// <summary>
    /// Gets symmetric matchings.
    /// </summary>
    /// <param name="edges">
    /// The graph edges.
    /// </param>
    /// <param name="count">
    /// The number of matchings to make.
    /// </param>
    /// <returns>
    /// The matchings.
    /// </returns>
    int[,] GetSymmetricMatchings(int[][] edges, int count);

    #endregion
  }

  /// <summary>
  /// The graph service.
  /// </summary>
  public class GraphService : IGraphService {
    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="GraphService"/> class.
    /// </summary>
    /// <param name="arrayService">
    /// The ArrayService.
    /// </param>
    public GraphService(IArrayService arrayService) {
      this.ArrayService = arrayService;
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets the array service.
    /// </summary>
    public IArrayService ArrayService { get; private set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Gets asymmetric matchings.
    /// </summary>
    /// <param name="edges">
    /// The graph edges.
    /// </param>
    /// <param name="count">
    /// The number of matchings to make.
    /// </param>
    /// <returns>
    /// The matchings.
    /// </returns>
    public int[,] GetAsymmetricMatchings(int[][] edges, int count) {
      edges = (from array in edges select ArrayModule.Copy(array)).ToArray();
      var result = new int[edges.Length, count];

      foreach (var iteration in Enumerable.Range(0, count)) {
        var scrambling = Enumerable.Range(0, edges.Length).ToArray();
        this.ArrayService.Shuffle(scrambling);
        var forward = Enumerable.Range(0, edges.Length).ToDictionary(i => i, i => scrambling[i] + edges.Length);
        var back = Enumerable.Range(0, edges.Length).ToDictionary(i => scrambling[i] + edges.Length);

        var firstHalf =
          (from i in Enumerable.Range(0, edges.Length)
           select (from n in edges[i] let m = forward[n] orderby m select m).ToArray()).ToArray();
        var secondHalf =
          (from i in Enumerable.Range(0, edges.Length)
           from j in firstHalf[i]
           group i by j into b
           orderby b.Key
           select b.ToArray()).ToArray();

        var matching = GraphModule.findPerfectMatching(this.CreateGraph(ArrayModule.Append(firstHalf, secondHalf)));
        if (OptionModule.IsNone(matching)) {
          throw new InvalidOperationException(
            string.Format("There is no perfect matching in iteration {0}/{1}", iteration, count));
        }

        foreach (var pair in matching.Value) {
          var left = Math.Min(pair.Item1, pair.Item2);
          var right = back[Math.Max(pair.Item1, pair.Item2)];
          result[left, iteration] = right;
          edges[left] = (from item in edges[left] where item != right select item).ToArray();
        }
      }

      return result;
    }

    /// <summary>
    /// Gets non-reciprocical matchings.
    /// </summary>
    /// <param name="edges">
    /// The graph edges.
    /// </param>
    /// <param name="count">
    /// The number of matchings to make.
    /// </param>
    /// <returns>
    /// The matchings.
    /// </returns>
    public int[,] GetNonReciprocicalMatchings(int[][] edges, int count) {
      edges = (from array in edges select ArrayModule.Copy(array)).ToArray();
      var result = new int[edges.Length, count];

      foreach (var iteration in Enumerable.Range(0, count)) {
        foreach (var array in edges) {
          this.ArrayService.Shuffle(array);
        }

        var evenEdges = edges;
        var cut = edges.Length;
        var odd = edges.Length % 2 == 1;
        if (odd) {
          cut = this.ArrayService.Some(Enumerable.Range(0, edges.Length));
          evenEdges = (from left in Enumerable.Range(0, edges.Length)
                       where left != cut
                       let items =
                         (from right in edges[left] where right != cut select right < cut ? right : right - 1)
                       select items.ToArray()).ToArray();
        }

        var lefts = new HashSet<int>();
        var rights = new HashSet<int>();
        Action<int, int> applyAssignment = (left, right) => {
          if (lefts.Contains(left)) {
            throw new InvalidOperationException("Left node was already assigned");
          }

          if (rights.Contains(right)) {
            throw new InvalidOperationException("Right node was already assigned");
          }

          lefts.Add(left);
          rights.Add(right);
          result[left, iteration] = right;
          edges[left] = (from item in edges[left] where item != right select item).ToArray();
          edges[right] = (from item in edges[right] where item != left select item).ToArray();
        };

        var firstMatching = GraphModule.findPerfectMatching(this.CreateGraph(evenEdges));
        if (OptionModule.IsNone(firstMatching)) {
          throw new InvalidOperationException(
            string.Format("There is no perfect first matching in iteration {0}/{1}", iteration, count));
        }

        foreach (var pair in firstMatching.Value) {
          applyAssignment(
            pair.Item1 < cut ? pair.Item1 : pair.Item1 + 1,
            pair.Item2 < cut ? pair.Item2 : pair.Item2 + 1);
        }

        if (odd) {
          // Handle cut as left
          applyAssignment(cut, this.ArrayService.Some(edges[cut].Except(rights)));

          // Handle cut as right
          applyAssignment(this.ArrayService.Some(edges[cut].Except(lefts)), cut);
        }

        var unequalBack =
          (from item in Enumerable.Range(0, edges.Length)
           where !lefts.Contains(item) || !rights.Contains(item)
           select item).ToArray();
        var unequalForward = Enumerable.Range(0, unequalBack.Length).ToDictionary(i => unequalBack[i]);
        var unequalEdges = from left in unequalBack
                           let allowed = from right in edges[left].Intersect(unequalBack)
                                         where
                                           (!lefts.Contains(left) && !rights.Contains(right))
                                           || (!rights.Contains(left) && !lefts.Contains(right))
                                         select unequalForward[right]
                           select allowed.ToArray();

        var secondMatching = GraphModule.findPerfectMatching(this.CreateGraph(unequalEdges.ToArray()));
        if (OptionModule.IsNone(secondMatching)) {
          throw new InvalidOperationException(
            string.Format("There is no perfect second matching in iteration {0}/{1}", iteration, count));
        }

        foreach (var pair in secondMatching.Value) {
          var item1 = unequalBack[pair.Item1];
          var item2 = unequalBack[pair.Item2];
          var left = lefts.Contains(item1) ? item2 : item1;
          var right = lefts.Contains(item1) ? item1 : item2;
          applyAssignment(left, right);
        }
      }

      return result;
    }

    /// <summary>
    /// Gets symmetric matchings.
    /// </summary>
    /// <param name="edges">
    /// The graph edges.
    /// </param>
    /// <param name="count">
    /// The number of matchings to make.
    /// </param>
    /// <returns>
    /// The matchings.
    /// </returns>
    public int[,] GetSymmetricMatchings(int[][] edges, int count) {
      edges = (from array in edges select ArrayModule.Copy(array)).ToArray();
      var result = new int[edges.Length, count];

      foreach (var iteration in Enumerable.Range(0, count)) {
        var matching = GraphModule.findPerfectMatching(this.CreateGraph(edges));
        if (OptionModule.IsNone(matching)) {
          throw new InvalidOperationException(
            string.Format("There is no perfect matching in iteration {0}/{1}", iteration, count));
        }

        foreach (var pair in matching.Value) {
          result[pair.Item1, iteration] = pair.Item2;
          edges[pair.Item1] = (from item in edges[pair.Item1] where item != pair.Item2 select item).ToArray();
          result[pair.Item2, iteration] = pair.Item1;
          edges[pair.Item2] = (from item in edges[pair.Item2] where item != pair.Item1 select item).ToArray();
        }
      }

      return result;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Creates a graph from a nested array.
    /// </summary>
    /// <param name="edges">
    /// The edges.
    /// </param>
    /// <returns>
    /// The graph.
    /// </returns>
    private FSharpList<Tuple<int, FSharpList<int>>> CreateGraph(int[][] edges) {
      return
        (from i in Enumerable.Range(0, edges.Length) select edges.Length - 1 - i).Aggregate(
          FSharpList<Tuple<int, FSharpList<int>>>.Empty, 
          (current, i) =>
          new FSharpList<Tuple<int, FSharpList<int>>>(
            new Tuple<int, FSharpList<int>>(i, ListModule.OfArray(edges[i])), 
            current));
    }

    #endregion
  }
}