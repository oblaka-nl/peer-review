﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Globalization;
  using System.IO;
  using System.Linq;

  /// <summary>
  /// The AssignmentService interface.
  /// </summary>
  public interface IAssignmentService {
    #region Public Methods and Operators

    /// <summary>
    /// Tries to add assignment sheet to workbook.
    /// </summary>
    /// <param name="workbook">
    /// The workbook.
    /// </param>
    /// <param name="message">
    /// The error or success message.
    /// </param>
    /// <returns>
    /// Whether the operation was succesful.
    /// </returns>
    bool TryAddAssignment(IExcelWorkbook workbook, out string message);

    #endregion
  }

  /// <summary>
  /// The assignment service.
  /// </summary>
  public class AssignmentService : IAssignmentService {
    #region Constants

    /// <summary>
    /// The review count.
    /// </summary>
    public const int ReviewCount = 5;

    /// <summary>
    /// The chunk size.
    /// </summary>
    internal const double ChunkSize = 200;

    #endregion

    #region Static Fields

    /// <summary>
    /// The assignment columns.
    /// </summary>
    internal static readonly string[] AssignmentColumns =
      new[] { "Reviewer", "PIN" }.Concat(
        Enumerable.Range(1, ReviewCount).Select(i => string.Format(CultureInfo.InvariantCulture, "Aanvraag-{0}", i)))
        .ToArray();

    /// <summary>
    /// All possible pins not starting with zero and 4 distinct digits.
    /// </summary>
    private static readonly int[] Pins = (from i in Enumerable.Range(1, 9)
                                          from j in Enumerable.Range(0, 10)
                                          where i != j
                                          from k in Enumerable.Range(0, 10)
                                          where i != k && j != k
                                          from l in Enumerable.Range(0, 10)
                                          where i != l && j != l && k != l
                                          select Int32.Parse(string.Concat(i, j, k, l))).ToArray();

    #endregion

    #region Fields

    /// <summary>
    /// The applicant factory.
    /// </summary>
    private readonly IApplicantFactory applicantFactory;

    /// <summary>
    /// The graph service.
    /// </summary>
    private readonly IGraphService graphService;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="AssignmentService"/> class.
    /// </summary>
    /// <param name="graphService">
    /// The graph service.
    /// </param>
    /// <param name="applicantFactory">
    /// The applicant factory.
    /// </param>
    public AssignmentService(IGraphService graphService, IApplicantFactory applicantFactory) {
      this.graphService = graphService;
      this.applicantFactory = applicantFactory;
    }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Tries to add assignment sheet to workbook.
    /// </summary>
    /// <param name="workbook">
    /// The workbook.
    /// </param>
    /// <param name="message">
    /// The error or success message.
    /// </param>
    /// <returns>
    /// Whether the operation was succesful.
    /// </returns>
    public bool TryAddAssignment(IExcelWorkbook workbook, out string message) {
      string innerMessage = null;
      Func<bool> tryAddAssignment = () => {
        string[,] submissions;
        try {
          var submissionColumns = new[] { "Kenmerk", "Postcode", "Plaats" };
          submissions = this.ExecuteWithLog(
            () => workbook.ReadSheet("Aanvragen", submissionColumns),
            "TryAddAssignment-ReadAanvragen");
        }
        catch (InvalidDataException e) {
          innerMessage = e.Message;
          return false;
        }

        var applicants = (from i in Enumerable.Range(0, submissions.GetLength(0))
                          where
                            Enumerable.Range(0, submissions.GetLength(1))
                            .Any(j => !string.IsNullOrWhiteSpace(submissions[i, j]))
                          select
                            this.applicantFactory.CreateApplicant(submissions[i, 0], submissions[i, 1], submissions[i, 2]))
          .ToArray();
        if (!applicants.Any()) {
          innerMessage = "Er zijn geen aanvragen.";
          return false;
        }

        if (applicants.Any(a => string.IsNullOrEmpty(a.Id))) {
          innerMessage = "Alle aanvragen moeten een uniek kenmerk hebben.";
          return false;
        }

        var doublures =
          (from applicant in applicants
           group applicant by applicant.Id into g
           where g.Count() > 1
           select g.Key).ToArray();
        if (doublures.Any()) {
          innerMessage = "Alle aanvragen moeten een uniek kenmerk hebben. Dubbele kenmerken gevonden: "
                    + string.Join(", ", doublures);
          return false;
        }

        try {
          Action determineLocations = () => {
            foreach (var applicant in applicants) {
              applicant.DetermineLocation();
            }
          };
          this.ExecuteWithLog(
            determineLocations,
            "TryAddAssignment-DetermineLocations",
            new { Count = applicants.Length });
        }
        catch (Exception e) {
          innerMessage = "De plaatsbepaling is mislukt. " + e.Message;
          return false;
        }

        var assignments = new string[applicants.Length, AssignmentColumns.Length];

        var chunks = Math.Max(1, (int)Math.Round(applicants.Length / ChunkSize));
        var chunkSize = (int)Math.Ceiling(applicants.Length / (double)chunks);
        var hasReciprocity = false;
        foreach (var chunk in Enumerable.Range(0, chunks)) {
          var start = chunk * chunkSize;
          var count = Math.Min(start + chunkSize, applicants.Length) - start;

          int[,] matching;
          var edges = (from i in Enumerable.Range(start, count)
                       select
                         (from j in Enumerable.Range(start, count)
                          where applicants[i].CanReview(applicants[j])
                          select j - start).ToArray()).ToArray();
          try {
            matching = this.ExecuteWithLog(
              () => this.graphService.GetNonReciprocicalMatchings(edges, ReviewCount),
              "TryAddAssignment-CalculateMatchings",
              new { Chunk = chunk });
          }
          catch {
            hasReciprocity = true;
            try {
              matching = this.ExecuteWithLog(
                () => this.graphService.GetAsymmetricMatchings(edges, ReviewCount),
                "TryAddAssignment-CalculateMatchings-AllowReciprocity",
                new {
                  Chunk = chunk
                });
            }
            catch (Exception e) {
              innerMessage = "Voor deze combinatie van aanvragers is geen toewijzing mogelijk. " + e.Message;
              return false;
            }
          }

          var pins = this.graphService.ArrayService.Sample(Pins, count);
          this.graphService.ArrayService.Shuffle(pins);
          foreach (var applicantIndex in Enumerable.Range(start, count)) {
            assignments[applicantIndex, 0] = applicants[applicantIndex].Id;
            assignments[applicantIndex, 1] = pins[applicantIndex - start].ToString(CultureInfo.InvariantCulture);
            foreach (var reviewIndex in Enumerable.Range(0, ReviewCount)) {
              assignments[applicantIndex, 2 + reviewIndex] = applicants[start + matching[applicantIndex - start, reviewIndex]].Id;
            }
          }
        }

        workbook.AddSheet("Toewijzingen", AssignmentColumns, assignments);
        innerMessage = "Toewijzing succesvol afgerond." + (hasReciprocity ? " Sommige reviewers moeten de aanvraag van de reviewer van hun eigen aanvraag beoordelen." : string.Empty);
        return true;
      };
      try {
        return this.ExecuteWithLog(tryAddAssignment, "TryAddAssignment");
      }
      finally {
        message = innerMessage;
      }
    }

    #endregion
  }
}