﻿namespace Oblaka.PeerReview.Services {
  using System;

  /// <summary>
  /// The Applicant interface.
  /// </summary>
  public interface IApplicant {
    #region Public Properties

    /// <summary>
    /// Gets the id.
    /// </summary>
    string Id { get; }

    /// <summary>
    /// Gets the locality.
    /// </summary>
    string Locality { get; }

    /// <summary>
    /// Gets the location.
    /// </summary>
    IPoint Location { get; }

    /// <summary>
    /// Gets the postal code.
    /// </summary>
    string PostalCode { get; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Checks if applicant can review submission of other application.
    /// </summary>
    /// <param name="other">
    /// The other applicant.
    /// </param>
    /// <returns>
    /// Whether applicant can review submission of other application.
    /// </returns>
    bool CanReview(IApplicant other);

    /// <summary>
    /// Determines the location.
    /// </summary>
    void DetermineLocation();

    #endregion
  }

  /// <summary>
  /// The applicant.
  /// </summary>
  internal class Applicant : IApplicant {
    #region Constants

    /// <summary>
    /// The minimum distance between localities.
    /// </summary>
    private const decimal MinimumDistance = 15E3M;

    #endregion

    #region Fields

    /// <summary>
    /// The lazy location.
    /// </summary>
    private readonly Lazy<IPoint> location;

    #endregion

    #region Constructors and Destructors

    /// <summary>
    /// Initializes a new instance of the <see cref="Applicant"/> class.
    /// </summary>
    /// <param name="id">
    /// The applicant id.
    /// </param>
    /// <param name="postalCode">
    /// The postal code.
    /// </param>
    /// <param name="locality">
    /// The locality.
    /// </param>
    /// <param name="geoService">
    /// The GeoService.
    /// </param>
    public Applicant(string id, string postalCode, string locality, IGeoService geoService) {
      this.Id = id;
      this.PostalCode = postalCode;
      this.Locality = locality;
      this.GeoService = geoService;
      this.location =
        new Lazy<IPoint>(
          () =>
          (string.IsNullOrWhiteSpace(this.PostalCode) ? null : this.GeoService.CodeByPostalCode(this.PostalCode))
          ?? (string.IsNullOrWhiteSpace(this.Locality) ? null : this.GeoService.CodeByLocality(this.Locality)));
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets the id.
    /// </summary>
    public string Id { get; private set; }

    /// <summary>
    /// Gets the locality.
    /// </summary>
    public string Locality { get; private set; }

    /// <summary>
    /// Gets the location.
    /// </summary>
    public IPoint Location {
      get {
        return this.location.Value;
      }
    }

    /// <summary>
    /// Gets the postal code.
    /// </summary>
    public string PostalCode { get; private set; }

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets the geo service.
    /// </summary>
    private IGeoService GeoService { get; set; }

    #endregion

    #region Public Methods and Operators

    /// <summary>
    /// Checks if applicant can review submission of other application.
    /// </summary>
    /// <param name="other">
    /// The other applicant.
    /// </param>
    /// <returns>
    /// Whether applicant can review submission of other application.
    /// </returns>
    public bool CanReview(IApplicant other) {
#if ALLOWALL
      return other != null && this.Id != other.Id;
#else
      return other != null && this.Id != other.Id && (this.Location != null || other.Location != null)
             && (this.Location == null || other.Location == null
                 || this.Location.DistanceTo(other.Location) > MinimumDistance);
#endif
    }

    /// <summary>
    /// Determines the location.
    /// </summary>
    public void DetermineLocation() {
      var location = this.location.Value;
    }

    #endregion
  }
}