﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Globalization;
  using System.Xml.Serialization;

  using Oblaka.PeerReview.Services.Pdok;

  /// <summary>
  /// The point.
  /// </summary>
  [XmlRoot(ElementName = "Point", Namespace = GeocodeResponse.GmlNamespace)]
  public class Point : IPoint {
    #region Public Properties

    /// <summary>
    /// Gets or sets the position.
    /// </summary>
    [XmlElement(ElementName = "pos", Namespace = GeocodeResponse.GmlNamespace)]
    public string Position { get; set; }

    /// <summary>
    /// Gets the x coordinate.
    /// </summary>
    public decimal X {
      get {
        return Convert.ToDecimal(this.Position.Split(' ')[0], CultureInfo.InvariantCulture);
      }
    }

    /// <summary>
    /// Gets the y coordinate.
    /// </summary>
    public decimal Y {
      get {
        return Convert.ToDecimal(this.Position.Split(' ')[1], CultureInfo.InvariantCulture);
      }
    }

    #endregion

    /// <summary>
    /// Gets a point from a position.
    /// </summary>
    /// <param name="position">
    /// The position.
    /// </param>
    /// <returns>
    /// The point or null.
    /// </returns>
    public static Point FromPosition(string position) {
      return string.IsNullOrWhiteSpace(position) ? null : new Point {
        Position = position
      };
    }
  }
}