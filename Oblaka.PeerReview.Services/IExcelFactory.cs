﻿namespace Oblaka.PeerReview.Services {
  using System.IO;

  /// <summary>
  /// The ExcelFactory interface.
  /// </summary>
  public interface IExcelFactory {
    #region Public Methods and Operators

    /// <summary>
    /// Reads excel workbook from stream.
    /// </summary>
    /// <param name="stream">
    /// The stream.
    /// </param>
    /// <returns>
    /// The <see cref="IExcelWorkbook"/>.
    /// </returns>
    IExcelWorkbook Read(Stream stream);

    #endregion
  }
}