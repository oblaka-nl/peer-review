﻿namespace Oblaka.PeerReview.Services {
  using System;
  using System.Collections.Generic;
  using System.Linq;

  /// <summary>
  /// The non persistant geo storage.
  /// </summary>
  public class NonPersistantGeoStorage : IGeoStorage {
    #region Public Methods and Operators

    /// <summary>
    /// Loads previously stored geocode results.
    /// </summary>
    /// <returns>
    /// The previously stored geocode results.
    /// </returns>
    public IEnumerable<Tuple<GeoType, string, IPoint>> Load() {
      return Enumerable.Empty<Tuple<GeoType, string, IPoint>>();
    }

    /// <summary>
    /// Saves a geocode result.
    /// </summary>
    /// <param name="geoType">
    /// The geo type.
    /// </param>
    /// <param name="argument">
    /// The argument.
    /// </param>
    /// <param name="point">
    /// The point.
    /// </param>
    public void Save(GeoType geoType, string argument, IPoint point) {
    }

    #endregion
  }
}